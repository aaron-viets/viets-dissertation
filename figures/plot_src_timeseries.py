
import sys
import os
import numpy
import time
from math import pi
import resource
import datetime
import time
import matplotlib
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 20
matplotlib.rcParams['legend.fontsize'] = 16
matplotlib.rcParams['mathtext.default'] = 'regular'
matplotlib.use('Agg')
import glob
import matplotlib.pyplot as plt

from optparse import OptionParser, Option
import ConfigParser

fs_data = []
q_data = []

fs_data.append(numpy.loadtxt("H1_fs_srcline.txt"))
fs_data.append(numpy.loadtxt("H1_fs_actline.txt"))
fs_data.append(numpy.loadtxt("L1_fs_actline.txt"))

q_data.append(numpy.loadtxt("H1_Qinv_srcline.txt"))
q_data.append(numpy.loadtxt("H1_Qinv_actline.txt"))
q_data.append(numpy.loadtxt("L1_Qinv_actline.txt"))

labels = ["H1 using 7.93 Hz line", "H1 using 36.7 Hz line", "L1 using 16.3 Hz line"]
colors = ['darkred', 'salmon', 'royalblue']

t_start = fs_data[2][0][0]
dur = fs_data[2][len(fs_data[2]) - 1][0] - t_start
t_unit = 'seconds'
sec_per_t_unit = 1.0
if dur > 60 * 60 * 100:
	t_unit = 'days'
	sec_per_t_unit = 60.0 * 60.0 * 24.0
elif dur > 60 * 100:
	t_unit = 'hours'
	sec_per_t_unit = 60.0 * 60.0
elif dur > 100:
	t_unit = 'minutes'
	sec_per_t_unit = 60.0
times = []
fs = []
q = []
for k in range(0, len(fs_data[2])):
	times.append((fs_data[2][k][0] - t_start) / sec_per_t_unit)

for i in range(0, 3):
	fs.append([])
	q.append([])
	for k in range(0, len(fs_data[2])):
		fs[i].append(fs_data[i][k][1])
		q[i].append(q_data[i][k][1])

	# Make plots
	if i == 0:
		plt.figure(figsize = (10, 10))
	plt.subplot(211)
	plt.plot(times, fs[i], colors[i % 3], linewidth = 1.0, label = labels[i])
	leg = plt.legend(fancybox = True)
	leg.get_frame().set_alpha(0.8)
	plt.gca().set_xscale('linear')
	plt.gca().set_yscale('linear')
	if i == 0:
		#plt.title('SRC detuning 2017-08-03')
		plt.ylabel(r'$f_{\rm s} \ {\rm [Hz]}$')
	#plt.xlim()
	plt.ylim(0.0, 8.0)
	plt.grid(True, linestyle = '--', dashes = (6, 18), linewidth = 0.2, color = 'black')
	ax = plt.subplot(212)
	ax.set_xscale('linear')
	plt.plot(times, q[i], colors[i % 3], linewidth = 1.0)
	if i == 0:
		plt.ylabel(r'$1 \ / \ Q$')
		plt.xlabel(r'${\rm Time \ in \ %s \ since \ %s \ UTC}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t_start + 315964782))))
	#plt.xlim()
	plt.ylim(-0.8, 0.4)
	plt.grid(True, linestyle = '--', dashes = (6, 18), linewidth = 0.2, color = 'black')
plt.savefig('SRC_detuning_2017-08-03.png')
plt.savefig('SRC_detuning_2017-08-03.pdf')




import numpy as np
from scipy import signal
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 14
matplotlib.rcParams['legend.fontsize'] = 14
matplotlib.rcParams['mathtext.default'] = 'regular'
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{color} \usepackage{xcolor} \usepackage{tcolorbox}')
import matplotlib.gridspec as gridspec

theta = np.arange(0, 2 * np.pi, np.pi / 500)
phi = np.arange(0, 2 * np.pi, np.pi / 500)
r_of_theta = 0.5 * (1.0 + np.cos(theta) * np.cos(theta))
r_of_phi = abs(np.cos(2 * phi))

r_of_theta_x = abs(np.cos(theta))
r_of_phi_x = abs(np.sin(2 * phi))

Theta, Phi = np.meshgrid(theta, phi)
R = 0.5 * (1.0 + np.cos(Theta) * np.cos(Theta)) * abs(np.cos(2 * Phi))
X = R * np.sin(Theta) * np.cos(Phi)
Y = R * np.sin(Theta) * np.sin(Phi)
Z = R * np.cos(Theta)

Rx = abs(np.cos(Theta) * np.sin(2 * Phi))
Xx = Rx * np.sin(Theta) * np.cos(Phi)
Yx = Rx * np.sin(Theta) * np.sin(Phi)
Zx = Rx * np.cos(Theta)

fig = plt.figure(figsize = (10, 12))
plt.gcf().subplots_adjust(bottom=0.25)

ax = plt.subplot(221, projection = '3d')
ax.plot_surface(X, Y, Z, rcount = 50, ccount = 50, cmap='Blues_r', edgecolors='k', lw=0.3, label = 'Plus')
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_zlabel(r'$z$')
ax.set_xlim(-0.5, 0.5)
ax.set_ylim(-0.5, 0.5)
ax.set_zlim(-1.0, 1.0)
ax.annotate('Plus', xy=(40, 320), xycoords='axes points', size=14, bbox=dict(boxstyle='round', fc='w'))

ax = plt.subplot(222, projection = '3d')
ax.plot_surface(Xx, Yx, Zx, rcount = 50, ccount = 50, cmap='Reds_r', edgecolors='k', lw=0.3, label = 'Cross')
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_zlabel(r'$z$')
ax.set_xlim(-0.5, 0.5)
ax.set_ylim(-0.5, 0.5)
ax.set_zlim(-1.0, 1.0)
ax.annotate('Cross', xy=(40, 320), xycoords='axes points', size=14, bbox=dict(boxstyle='round', fc='w'))

ax = plt.subplot(223, projection = 'polar')
ax.plot(theta, r_of_theta, 'blue', linewidth = 0.75, label = 'Plus')
ax.plot(theta, r_of_theta_x, 'red', linewidth = 0.75, label = 'Cross')
ax.set_theta_zero_location('N')
plt.yticks([0.2, 0.4, 0.6, 0.8, 1.0])
ax.set_xticklabels([r'0$^{\circ}$', r'45$^{\circ}$', r'90$^{\circ}$', r'135$^{\circ}$', r'180$^{\circ}$', r'135$^{\circ}$', r'90$^{\circ}$', r'45$^{\circ}$'])
plt.grid(True, linestyle = '--', dashes = (6, 18), linewidth = 0.2, color = 'black')
leg = plt.legend(fancybox = True, loc = 'upper right', bbox_to_anchor = (1.1, 0.85), edgecolor = 'black')
leg.get_frame().set_alpha(1.0)
ax.annotate('Polar dependence', xy=(0, 310), xycoords='axes points', size=14, bbox=dict(boxstyle='round', fc='w'))

ax = plt.subplot(224, projection = 'polar')
ax.plot(phi, r_of_phi, 'blue', linewidth = 0.75, label = 'Plus')
ax.plot(phi, r_of_phi_x, 'red', linewidth = 0.75, label = 'Cross')
plt.tight_layout()
plt.yticks([0.2, 0.4, 0.6, 0.8, 1.0])
plt.grid(True, linestyle = '--', dashes = (6, 18), linewidth = 0.2, color = 'black')
ax.annotate('Azimuthal dependence', xy=(-20, 310), xycoords='axes points', size=14, bbox=dict(boxstyle='round', fc='w'))

plt.savefig('LIGO_antenna_pattern.pdf')





import numpy as np
from scipy import signal

import matplotlib
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['legend.fontsize'] = 14
matplotlib.rcParams['mathtext.default'] = 'regular'
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)

import FIRfreqResponse as FIR
from ticks_and_grid import ticks_and_grid


def plot_window_fft(windows, sr, labels, filename = "window_fft.png"):
	if type(windows) is not list:
		windows = [windows]
	if type(labels) is not list:
		labels = [labels]
	for i in range(len(windows) - len(labels)):
		labels.append(labels[-1])

	times = []
	freqs = []
	freqresps = []
	fmax_zoom = 0.0
	magmin_zoom = 1.0
	for i in range(len(windows)):
		# Time vectors
		dur = float(len(windows[i])) / sr
		t = np.arange(0, dur, 1.0 / sr)
		t = t - t[-1] / 2.0
		times.append(t)

		# Frequency vectors
		freq = np.fft.rfftfreq(8 * len(t), d = 1.0 / sr)
		for j in range(1, len(freq)):
			freq = np.insert(freq, 0, -freq[2 * j - 1])
		freqs.append(freq)

		# Magnitudes of frequency responses of each window
		freqresp = abs(FIR.freqresp(windows[i]))
		freqresp /= max(freqresp)
		for j in range(1, len(freqresp)):
			freqresp = np.insert(freqresp, 0, freqresp[2 * j - 1])
		freqresps.append(freqresp)

		# Find the width of the main lobe to decide plot limits.
		df = 1.0 / dur / 8.0
		fmax = 0.0
		magmin = 1.0
		for j in range(len(freqresp) // 2, len(freqresp) - 1):
			if freqresp[j + 1] > freqresp[j]:
				fmax = 5 * df * (j - len(freqresp) // 2)
				magmin = np.percentile(freqresp[len(freqresp) // 2 : len(freqresp) // 2 + 5 * (j - len(freqresp) // 2)], 1)
				break
		fmax_zoom = max(fmax_zoom, fmax)
		magmin_zoom = min(magmin_zoom, magmin)

	# Now make the figure with three plots:
	colors = ['royalblue', 'maroon', 'springgreen', 'red', 'gold', 'magenta', 'orange', 'aqua', 'darkgreen', 'blue']
	plt.figure(figsize = (12, 4))
	plt.gcf().subplots_adjust(bottom=0.25)
	ax = plt.subplot(131)
	for i in range(len(windows)):
		plt.plot(times[i], windows[i], colors[i % 10], linewidth = 0.75)
	plt.ylabel('Magnitude')
	plt.xlabel('Time (s)')
	ticks_and_grid(ax, xscale = 'linear', yscale = 'linear')

	ax = plt.subplot(132)
	for i in range(len(windows)):
		plt.plot(freqs[i], freqresps[i], colors[i % 10], linewidth = 0.75, label = labels[i])
	leg = plt.legend(fancybox = True, loc = 'upper right')
	leg.get_frame().set_alpha(1.0)
	plt.xlabel('Frequency (Hz)')
	ticks_and_grid(ax, xscale = 'linear', yscale = 'log')

	ax = plt.subplot(133)
	for i in range(len(windows)):
		plt.plot(freqs[i], freqresps[i], colors[i % 10], linewidth = 0.75)
	plt.xlabel('Frequency (Hz)')
	ticks_and_grid(ax, xmin = -fmax_zoom, xmax = fmax_zoom, ymin = magmin_zoom, xscale = 'linear', yscale = 'log')

	plt.tight_layout()

	plt.savefig(filename)




import sys
import os
import numpy
import time
from math import pi
import resource
import datetime
import time
import matplotlib
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 14
matplotlib.rcParams['legend.fontsize'] = 16
matplotlib.rcParams['mathtext.default'] = 'regular'
matplotlib.use('Agg')
import glob
import matplotlib.pyplot as plt

from optparse import OptionParser, Option
import ConfigParser

data = numpy.loadtxt("freq1.txt")

colors = ['darkred', 'salmon', 'royalblue']

t_start = data[0][0]
dur = data[len(data) - 1][0] - t_start
t_unit = 'seconds'
sec_per_t_unit = 1.0
if dur > 60 * 60 * 100:
	t_unit = 'days'
	sec_per_t_unit = 60.0 * 60.0 * 24.0
elif dur > 60 * 100:
	t_unit = 'hours'
	sec_per_t_unit = 60.0 * 60.0
elif dur > 100:
	t_unit = 'minutes'
	sec_per_t_unit = 60.0
times = list(numpy.transpose(numpy.asarray(data))[0])
freqs = list(numpy.transpose(numpy.asarray(data))[1])

for i in range(len(times)):
	times[i] = (times[i] - t_start) / sec_per_t_unit

# Make plots
plt.figure(figsize = (9, 5))
plt.gcf().subplots_adjust(bottom=0.15)
plt.plot(times, freqs, colors[0], linewidth = 1.0)
plt.gca().set_xscale('linear')
plt.gca().set_yscale('linear')
plt.ylabel(r'${\rm Frequency \ [Hz]}$')
plt.ylim(59.96, 60.04)
plt.grid(True, which = "both", linestyle = '--', dashes = (6, 18), linewidth = 0.2, color = 'black')
plt.xlabel(r'${\rm Time \ in \ %s \ since \ %s \ UTC}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t_start + 315964782))))
plt.savefig('MainsmonFreqs.png')
plt.savefig('MainsmonFreqs.pdf')



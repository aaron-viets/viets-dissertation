\documentclass[VietsDissertation.tex]{subfiles}

The goal of computing the strain signal in the Advanced LIGO detectors is to calibrate GW signals.  However, the majority of the content of the calibrated $h(t)$ signal does not come from GWs, but from various sources of noise.  Some of these noise sources, such as the shot noise that dominates at high frequencies, cannot be distinguished from GWs due to the fact that they are only measured in DARM.  Other sources of noise can be measured by other witness sensors that are not sensitive to GWs.  Such witness sensors have been successfully used to subtract excess noise from $h(t)$ in high latency, significantly increasing detector sensitivity \cite{crossTalkRemoval, Jenne, Derek}.  After O2, an offline noise subtraction pipeline written in Python was used to subtract spectral lines and broadband noise over the entirety of the run \cite{Derek}.  This had an especially significant impact at H1, where excess motion in the laser beam contributed significant measureable noise to the $h(t)$ spectrum.  Removal of this noise was shown at many times to increase H1's total detectable volume for binary neutron star inspirals by a factor of 2.

\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/H1_1187004416_1187012608_spectrum_comparison_GW170817_allcorr.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.475\textwidth}
        \includegraphics[width=\textwidth]{figures/H1_1187004416_1187012608_ASD_ratios_GW170817_allcorr.pdf}
    \end{subfigure}
    \caption[Amplitude spectral density with noise subtraction]{
    Comparison of the three amplitude spectral densities computed from 8192 s of data around the time of the event GW170817.  The channel DCS-CALIB\_STRAIN contains $h(t)$ data produced in high latency.  DCH-CLEAN\_STRAIN\_C02 was produced in high latency after O2 by subtracting spectral lines and broadband noise.  Similar subtraction methods have since been developed in the \texttt{gstlal} calibration pipeline to produce the channel DCS-CALIB\_STRAIN\_CLEAN.
    \label{fig:noiseSubASDs}}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/H1_BNS_range_1187004416-8160.pdf}
    \caption[Binary neutron star inspiral range with noise subtraction]{
    Angle-averaged binary neutron star inspiral range computed from 8192 s of data around the time of the event GW170817, comparing three data sets.  The noise subtraction in the \texttt{gstlal} calibration pipeline (DCS-CALIB\_STRAIN\_CLEAN) accounts for a 210\% increase in total detectable volume.
    \label{fig:noiseSubBNSRange}}
\end{figure}

Since O2, a method for subraction of persistent spectral lines and broadband noise has been developed in the \texttt{gstlal} calibration pipeline.  Perhaps the most significant benefit of including broadband noise subtraction in the \texttt{gstlal} calibration pipeline is the resulting ability to provide noise-subtracted $h(t)$ data in very low latency.  A second benefit is that this method does not require the production of a new set of GW frame files; the cleaned $h(t)$ data is included in the GW frame files as they are produced.  Preliminary results show that this subtraction pipeline can provide cleaned data similar to that produced in high latency after O2, with negligible impact on latency.  Fig.~\ref{fig:noiseSubASDs} shows a comparison of the calibrated $h(t)$ data without subtraction, the cleaned $h(t)$ data produced after O2, and the noise subtraction done in the \texttt{gstlal} calibration pipeline, around the time of the binary neutron star merger GW170817.  Noise reduction in \texttt{gstlal} cleaned data is similar to that produced after O2 above $\sim$100 Hz, with improved reduction below $\sim$100 Hz.  This is primarily due to the fact that the O2 noise subtraction team elected not to use angular and length sensing control witness channels that couple to low-frequency noise, due to the fact that these did not have a large impact throughout the run.  Also note that the high-pass filtering shown here has improved since O2, which may impact noise subtraction at low frequencies.  The impact of the noise subtraction on the inspiral range (the average distance at which a binary neutron star system with each mass equal to 1.4 ${\rm M_{\odot}}$ can be computed with a signal-to-noise ratio of 8, as computed by \texttt{GWpy} \cite{GWpy}) during the same time is shown in Fig.~\ref{fig:noiseSubBNSRange}, where the subtraction in the \texttt{gstlal} calibration pipeline accounts for a 210\% increase in total detectable volume at H1.  Preliminary results shortly prior to O3 indicate a smaller impact, contributing an increase in detectable volume for the H1 detector of $\sim$5\%.

\section{Subtraction of calibration lines} \label{sec:SubCalLines}

Removal of loud spectral lines is not only useful for downstream data analysis such as searches for continuous waves and the GW stochastic background, it is also beneficial for the subtraction of broadband noise to follow.  Loud lines that are present in the $h(t)$ signal but not in the broadband noise witness sensors corrupt the calculation of transfer functions between witness channels and $h(t)$ at their frequencies, leading to the injection of noise around those lines.

To subtract calibration lines injected using the Pcal and the actuators at each stage, the \texttt{gstlal} calibration pipeline measures the amplitude and phase of the calibration lines in the injections channels $x_{\rm pc}$, $x_{\rm T}$, $x_{\rm P}$, and $x_{\rm U}$, and it then computes the corresponding sinusoidal excitation expected to occur in $h(t)$.  The Pcal lines are therefore subtracted under the assumption that the computed $h(t)$ is accurate relative to the Pcal.  The steps used to subtract Pcal lines are:
\begin{enumerate}
\item Demodulate the Pcal channel $x_{\rm pc}$ at the frequency of the calibration line being subtracted.  Letting $x_{\rm pc}(t) = a \cos(\omega t - \phi) + n(t)$, where $\omega$, $a$, and $\phi$ are the angular frequency, amplitude, and phase of the calibration line, respectively, and $n(t)$ is noise, the result of the demodulation is the factor $(a / 2) e^{-i \phi}$.
\item Convert this factor from counts in $\tilde{x}_{\rm pc}$ to meters in $\Delta \tilde{L}_{\rm free}$.  Note that the first two steps are often already done to compute the TDCFs, in which case it is possible to save computational cost by using the previous result.
\item Reconstruct a sinusoid to be subtracted from $h(t)$:
\begin{equation}
    \Re\left(2e^{i \omega t} \times \frac{a}{2} e^{-i \phi}\right) = a \cos(\omega t - \phi).
\end{equation}
\item Subtract from $h(t)$.
\end{enumerate}
Pcal lines could also be subtracted by applying an FIR filter to $x_{\rm pc}$ to convert it from counts to meters at all frequencies and then subtracting all the lines at once.  The reason for using the method is to save computational cost, especially in light of the fact that two of the three lines that are normally subtracted are already demodulated to compute the TDCFs.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/H1_Calibration_Line_Subtraction_1186099212-77583.pdf}
    \caption[Calibration line subtraction stability over time]{A time series of the ratio $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$ at the calibration line frequencies at H1 during O2.  $h(t)$ and $h_{\rm clean}(t)$ were demodulated using a 10-second low-pass filter, and the ratios were averaged for 128 seconds to produce each point.  Three pcal lines are shown at frequencies $f_1$ = 36.7 Hz, $f_2$ = 331.9 Hz, and $f_3$ = 1083.7 Hz.  The electrostatic drive actuator line at $f_{\rm T}$ = 35.9 Hz is also shown.  These were all the visible calibration lines in the detection band during O2.
    \label{fig:calLineSubTimeSeries}}
\end{figure}

The method used to subtract actuator lines from $h(t)$ is the same except that the conversion from counts to meters is time-dependent, due to fluctuations in the actuation strength.  A demodulated factor $(a_j / 2) e^{-i \phi_j}$, where $j \in \{T, P, U\}$, and $a_j$ and $\phi_j$ are the amplitude and phase as measured in the injection channel $x_j$, is converted from counts to meters using
\begin{equation}
    \frac{a}{2} e^{-i \phi} = \kappa_j e^{-i \omega_j \tau_j} a_j \times \frac{a_j}{2} e^{-i \phi_j}
\end{equation}
Note that the time-dependent calibration model is used to construct the actuator injections to be subtracted from $h(t)$.  In addition to saving computational cost using the fact that these lines have already been demodulated to compute the TDCFs, this can offer the benefit of applying the same time-dependent compensations to the line subtraction that is applied to correct $h(t)$.  Since much effort has been dedicated to compensating for time dependence in the calibration, and since the TDCFs computed by the \texttt{gstlal} calibration pipeline were shown to be stable and well-behaved throughout O2, we suggest that this method is a stable and safe way to subtract calibration lines, with very little risk of impacting GWs in the $h(t)$ signal.

For both Pcal lines and actuator lines, this method relies on the accuracy of the time-dependent calibration model.  It therefore can provide a continuous check of calibration accuracy at the calibration lines; in other words, systematic errors in the calibration will reduce the effectiveness of the subtraction.

The performance of the calibration line subtraction is shown over $\sim$22 hours of H1 data during O2 in Fig.~\ref{fig:calLineSubTimeSeries}.  The ratio $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$ is shown at each of the calibration line frequencies.  The lower reduction of the line height of the Pcal line at $f_3$ is likely due in part to the fact that this line has a lower signal-to-noise ratio than the others, but lower calibration accuracy may also contribute.  The clustering of the phase of $\tilde{h}_{\rm clean}(f_{\rm T}) / \tilde{h}(f_{\rm T})$ around 90$^{\circ}$ is evidence of a small systematic error in the calibration, possibly originating from the computed value of $\kappa_{\rm T}$ or $\tau_{\rm T}$.

At times when the calibration model is believed to be unreliable, the calibration lines can be subtracted using the same method that is used to subtract additional spectral lines, described in the next section.

\section{Subtraction of additional spectral lines} \label{sec:SubPowerLines}

In addition to calibration lines, the $h(t)$ spectrum also contains other spectral lines, such as 60-Hz power mains lines and harmonics, whose amplitude and phase can be estimated from correlated noise in witness sensors.  Unlike the case for the calibration lines, the transfer functions between these witness sensors and $h(t)$ are not thoroughly studied to produce an accurate time-dependent calibration model.  Moreover, such lines are generally much less stable than calibration lines, with frequent fluctuations in amplitude and frequency.

The general method used in the \texttt{gstlal} calibration pipeline to subtract spectral lines allows the use of multiple witness channels, in order to subtract as much noise as possible.\footnote{So far, the best results have been obtained using only one witness channel, but the general method is described for completeness.}  The steps in the process are described below:
\begin{enumerate}
\item Demodulate $h(t)$ and each witness channel $w_n$ at the expected frequency of the line to be subtracted.  If there are multiple line frequencies (e.g., harmonics) in a single witness channel to be subtracted, $w_n$ is demodulated at each frequency separately, and steps 1-4 are done for each frequency.
\item Take the ratios $\tilde{h}(f) / \tilde{w}_n(f)$ and $\tilde{w}_m / \tilde{w}_n$.
\item Take a running median and a running average of the above ratios.  Currently, only a 128-second running median is used.
\item Due to the fact that different witness sensors may detect the same noise, care must be taken to avoid subtracting the same signal from $h(t)$ multiple times.  To accomplish this, we find ``optimized" transfer functions $T_n$ by solving the matrix equation
\begin{equation}
\begin{bmatrix}
    1                                     & \left<\frac{\tilde{w}_2}{\tilde{w}_1}\right>       & \dots  & \left<\frac{\tilde{w}_{\rm N}}{\tilde{w}_1}\right> \\
    \left<\frac{\tilde{w}_1}{\tilde{w}_2}\right>       & 1                                     & \dots  & \left<\frac{\tilde{w}_{\rm N}}{\tilde{w}_2}\right> \\
    \vdots                                & \vdots                                & \ddots & \vdots \\
    \left<\frac{\tilde{w}_1}{\tilde{w}_{\rm N}}\right> & \left<\frac{\tilde{w}_2}{\tilde{w}_{\rm N}}\right> & \dots  & 1
\end{bmatrix}
\begin{bmatrix}
    T_1 \\
    T_2 \\
    \vdots \\
    T_{\rm N}
\end{bmatrix}
=
\begin{bmatrix}
    \left<\frac{\tilde{h}}{\tilde{w}_1}\right> \\
    \left<\frac{\tilde{h}}{\tilde{w}_2}\right> \\
    \vdots \\
    \left<\frac{\tilde{h}}{\tilde{w}_{\rm N}}\right>
\end{bmatrix}
\end{equation}
at the frequencies of the spectral lines.  In order to prevent large errors in these transfer functions, the values of the transfer functions are updated only when the detector is in a nominal low-noise configuration.
\item
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/MainsmonFreqs.pdf}
    \caption[Temporal drifts in the power mains frequency]{
    A time series of the power mains fundamental frequency over time, as measured by the \texttt{gstlal} calibration pipeline.
    \label{fig:powerMainsFreq}}
\end{figure}
As shown in Fig.~\ref{fig:powerMainsFreq}, fluctuations of about $\pm$0.03 Hz are observed in the fundamental frequency of the power mains.  In general, the low-pass filter used in demodulating $h(t)$ and the witness channels is not centered in time (this is necessary in low latency, for instance).  Deviations in the true frequency of the line being subtracted from the expected frequency would therefore cause a phase error in the sinusoid that is constructed in the next step if this were not compensated for.  To correct this in the power mains subtraction, the fundamental frequency of the power mains, expected to be $\sim$60 Hz, is tracked continuously.  The algorithm that tracks the frequency does so by measuring the temporal separation between times when the witness-channel signal crosses zero, measured using linear interpolation.  The number of half-cycles to use for each measurement is set by the user, and the output is averaged to remove any ``kinks" from the resulting time series.  This provides a computationally cheap and accurate measure of frequency with a very fast response to changes over time.  It assumes, however, that the line being measured in the signal is loud compared to all other components of the signal.  If this is not the case, a short band-pass filter can be used before measurement.   The deviation of the measured frequency from the expected frequency is then used to produce corrective phase factors $\phi_{\rm corr}$ to be applied to the transfer functions $T_n$.  For the case of power mains lines, the measured fundamental frequency is used to compute a corrective phase factor for each harmonic.
\item The spectral lines are then reconstructed and subtracted from $h(t)$ using
\begin{equation}
    h_{\rm clean}(t) = h(t) - \sum_j \sum_n \Re\left(e^{i \omega_j t} \times \phi_{{\rm corr}, j} T_n(f_j) \tilde{w}_n(f_j)\right) \, ,
\end{equation}
where the $f_j$ are the expected frequencies of the lines being subtracted using a set of witness channels $w_n$.
\end{enumerate}
In general, this can be done using multiple sets of witness channels that detect different spectral lines.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/H1_Power_Mains_Line_Subtraction_1186099212-77583.pdf}
    \caption[Power mains line subtraction stability over time]{A time series of the ratio $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$ at 60 Hz and harmonics, showing the performance of the power mains line subtraction over time.  $h(t)$ and $h_{\rm clean}(t)$ were demodulated using a 10-second low-pass filter, and the ratios were averaged for 128 seconds to produce each point.
    \label{fig:powerLineSubTimeSeries}}
\end{figure}
The performance of the power mains line subtraction is shown in Fig.~\ref{fig:powerLineSubTimeSeries} using the same H1 data as was used in Fig.~\ref{fig:calLineSubTimeSeries}.  The ratio $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$ is shown at the first 3 harmonics of the fundamental frequency $f_1$ = 60 Hz.  The next 2 harmonics are also subtracted in the \texttt{gstlal} calibration pipeline, but the impact is increasingly smaller with each harmonic due to the fact that the higher harmonics are not as loud in the $h(t)$ spectrum.

\section{Subtraction of broadband noise in the \texttt{gstlal} calibration pipeline}

Witness sensors and other auxiliary channels can also be used to subtract broadband noise from the $h(t)$ signal, if they contain correlated noise and are insensitive to GWs.  During O2 at H1, jitter in the laser beam contributed significant noise from 100 Hz to 1kHz.  To measure this noise, a set of three split photodiodes was installed to measure beam motion and fluctuations in beam size \cite{Jenne}.  Each photodetector has a center photodiode, and three photodiodes of equal size arranged in a ring around the center photodiode.  Control of the length of the cavities and angluar motion of the mirrors also contributed noise at frequencies below a few tens of Hz.  The digital control signals sent to the actuators are used to subtract this noise.

The methods developed in the \texttt{gstlal} calibration pipeline to subtract broadband noise in $h(t)$ assume that the noise in the witness channels is linearly coupled to $h(t)$ and that it is stationary on the tens-of-minutes timescales typically used to compute transfer functions between these witness sensors and $h(t)$.  

\subsection{Computing transfer functions}

The first step necessary to subtract noise contributions from the chosen witnesses from the $h(t)$ signal is to estimate transfer functions that minimize the RMS of the signal
\begin{equation}\label{eq:optimizedTFdefinition}
    \tilde{h}_{\rm clean}(f) = \tilde{h}(f) - \sum_n T_n(f)\tilde{w}_n(f).
\end{equation}
We begin by applying short Hann windows to time-domain $h(t)$ data and witness channel data and taking Fourier transforms to compute $\tilde{h}(f)$ and $\tilde{w}_n(f)$ for each segment of data.  Each window is overlapped with the previous window by half the length of the window so as to weight each moment in time equally while still avoiding edge effects.  Any frequency bands that are known beforehand to have loud spectral lines in $h(t)$ that are not present in the witness channels are then smoothed over by replacing the Fourier transforms with straight lines in those bands.  If this is not done, the transfer functions computed later may have errors around the frequencies of loud lines due to the large amount of noise in the transfer functions at those frequencies.  The ratios $\tilde{h}(f) / \tilde{w}_n(f)$ and $\tilde{w}_m(f) / \tilde{w}_n(f)$ are then taken from the DC component to the Nyquist frequency of the witness channels for each windowed segment of data separately.  Median values of these ratios are taken at all frequencies, in order to reduce noise in the transfer functions.  In order to achieve the desired temporal duration for the FIR filters produced later, these ratios are then resampled using a windowed sinc table.  This allows the flexibility of being able to average over noise in the time domain using a median or in the frequency domain.  The use of a median instead of a mean in the time domain is important, as it renders the result insensitive to occasional outliers due to, e.g., large short-duration glitches in the $h(t)$ data.  The ability to smooth the transfer functions in the frequency domain also has benefits, especially for cases in which witness channels are highly correlated.  In this case, the matrix in Eq.~\ref{eq:noiseSubMatrix} is ill-conditioned, and the solutions for the transfer functions can have large noisy fluctuations over frequency.

In the current configuration used in the low-latency \texttt{gstlal} calibration pipeline, 1000 4-second Fourier transforms are taken over a total time of 2002 seconds used to compute the median values of each ratio.  The length of the FIR filters is 1 second, meaning that the ratios $\tilde{h}(f) / \tilde{w}_n(f)$ and $\tilde{w}_m(f) / \tilde{w}_n(f)$ are downsampled by a factor of 4.

Similar to the case for the subtraction of spectral lines discussed in Section~\ref{sec:SubPowerLines}, witness channels used in broadband noise subtraction often are correlated to one another, and it is therefore necessary to find optimized transfer functions $T_n$ by solving
\begin{equation} \label{eq:noiseSubMatrix}
\begin{bmatrix}
    1                                     & \left<\frac{\tilde{w}_2}{\tilde{w}_1}\right>       & \dots  & \left<\frac{\tilde{w}_{\rm N}}{\tilde{w}_1}\right> \\
    \left<\frac{\tilde{w}_1}{\tilde{w}_2}\right>       & 1                                     & \dots  & \left<\frac{\tilde{w}_{\rm N}}{\tilde{w}_2}\right> \\
    \vdots                                & \vdots                                & \ddots & \vdots \\
    \left<\frac{\tilde{w}_1}{\tilde{w}_{\rm N}}\right> & \left<\frac{\tilde{w}_2}{\tilde{w}_{\rm N}}\right> & \dots  & 1
\end{bmatrix}
\begin{bmatrix}
    T_1 \\
    T_2 \\
    \vdots \\
    T_{\rm N}
\end{bmatrix}
=
\begin{bmatrix}
    \left<\frac{\tilde{h}}{\tilde{w}_1}\right> \\
    \left<\frac{\tilde{h}}{\tilde{w}_2}\right> \\
    \vdots \\
    \left<\frac{\tilde{h}}{\tilde{w}_{\rm N}}\right>
\end{bmatrix} \, ,
\end{equation}
this time at each frequency in the transfer functions.  To insure accuracy in each estimate of the optimized transfer functions, new Fourier transforms are computed only when the detector is in a nominal low-noise configuration.

\subsection{Producing and updating FIR filters for witness channels}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/cleaning_pipeline_diagram.pdf}
    \caption[Block diagram of noise subtraction in the \texttt{gstlal} calibration pipeline]{
    An example showing the workflow of broadband noise subtraction in the \texttt{gstlal} calibration pipeline.  Solid lines represent the flow of time series data, while dashed lines represent the periodic passing of FIR filters from elements that compute them to elements that apply them.  In this exmaple, two iterations of noise subtraction are done, the first processing three witness channels in parallel, and the second processing two witness channels in parallel.  The block labeled ``Compute $h(t)$" represents the primary component of the \texttt{gstlal} calibration pipeline described in Ch.~\ref{ch:GstlalCalibration} and in Figs.~\ref{fig:GDSpipeline} and~\ref{fig:DCSpipeline}.
    \label{fig:cleaningPipeline}}
\end{figure}
The optimized transfer functions $T_n$ can be applied to the witness channels $w_n$ to subtract their noise contributions from $h(t)$ as described by Eq.~\ref{eq:optimizedTFdefinition}.  In order to do this in the \texttt{gstlal} calibration pipeline, they must be used to produce FIR filters to be applied to the witness channels.  To do this, we first apply any high- or low-pass filters by rolling the $T_n$ off in the frequency domain.  Currently, a high-pass filter with a 10-Hz corner frequency is applied to prevent the addition of any noise in $h(t)$ below 10 Hz.  A delay of half the length of the filter is then added in order to center the filter in time, making it non-causal.  Then an inverse Fourier transform is taken to produce a time-domain FIR filter.  The edges of the filter are smoothed off using a Tukey window in the time domain.  These filters are sent to a filtering algorithm that smoothly handles filter updates by windowing out the output of the previous filter using half of a Hann window while windowing in the output of the new filter over some transition time set by the user.  These filters are updated each time a new set of transfer functions is computed from a new data set.

If desired, multiple iterations of noise subtraction can be done, i.e., after subtracting noise from $h(t)$ using one set of witness channels, another set of witness channels can be used to do additional cleaning.  It is important to note that, in order to avoid subtracting the same signal multiple times, transfer functions produced for later iterations of noise subtraction must be computed using cleaned $h(t)$ data from previous iterations of subtraction.  Results from the noise subtraction in the \texttt{gstlal} calibration pipeline have shown benefits from processing witness channels together using Eq.~\ref{eq:noiseSubMatrix} primarily when those witness channels measure noise in the same frequency band.  For sets of witness channels that do not measure noise in the same frequency band, results were improved by subtracting noise computed from those witness channels using separate iterations of the noise subtraction in series, thus reducing the size of the matrix of Eq.~\ref{eq:noiseSubMatrix} in each iteration.  The subtraction done using H1 data from O2 shown in this section used two iterations of cleaning, starting with 7 witness sensors used to detect beam jitter from $\sim$100 Hz to 1kHz, followed by 7 control signals for angular motion and length control, contributing noise below $\sim$100 Hz.  Fig.~\ref{fig:cleaningPipeline} shows an example of the noise subtraction in the \texttt{gstlal} calibration pipeline in the form of a block diagram. 

\subsection{Low-latency and high-latency noise subtraction}

Several key differences exist between the methods used in low latency versus those used in high latency.  In the low-latency \texttt{gstlal} calibration pipeline, the noise subtraction is configured so as to not add any latency to the $h(t)$ data stream.  The FIR filters being applied to witness channels are therefore computed using previous data, leading to possible concerns about the time-dependence of the transfer functions.  Results seen so far indicate that temporal variations are slow enough that updating the transfer functions about once per hour is sufficient to achieve effective subtraction.

In low latency, transfer functions are computed as soon as possible once the detector enters a low-noise state, if it has been a sufficiently long time (as determined by the user) since the last low-noise state.  Additionally, two previous transfer functions are stored at all times: one based on the most recent data, and one based on the beginning of the current or most recent low-noise strecth.  The reasoning behind this is that we expect the transfer functions at the beginning of a low-noise state to be more similar to ones computed at the beginning of the previous low-noise state than they are to the most recently computed transfer functions.

Unlike low-latency noise subtraction, high-latency noise subtraction can be done using FIR filters computed from a given segment of data to subtract noise from the same segment of data.  This constitutes a slight improvement over the low-latency noise subtraction, since the transfer functions are known to drift with time.  The high-latency noise subtraction needs to be done using many 4096-second jobs run in parallel, and it is therefore important that the end of each 4096-second chunk of cleaned strain data is continuous with the beginning of the next 4096-second chunk of cleaned strain data.  In order to accomplish this, it is necessary to compute the same transfer functions at the end of a job as those computed at the beginning of the next job.  As an example of how this can be done, consider the case of using 1024 s of data to compute each set of transfer functions.  During a 4096-second job, 5 sets of these transfer functions need to be produced, requiring an extra 512 s of raw data before the beginning and after the end of the 4096-second job, plus any additional time needed to produce the uncleaned strain data.  To ensure that the transfer functions computed from a segment of data are independent of start time, transfer functions are computed from predetermined time intervals set by the user and separated by 1024 s.  If the full 1024 s is not available during the job, the transfer functions will not be computed for that cycle.  Only data during low-noise states will be used, regardless of how much or how little low-noise data is availible during the predetermined 1024-second time window.

\subsection{Stability over time} \label{ssec:noiseSubStability}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/H1_transfer_functions_CLEAN_STRAIN_1186099200-77824.pdf}
    \caption[Transfer function between uncleaned strain data and cleaned strain data]{
    The transfer function $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$ computed using $\sim$22 hours of H1 data on August 7, 2017.  Fourier transforms of 16-second chunks of overlapped Hann-windowed data were used to compute the ratio $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$, and the median of this ratio was taken over 22 hours.  Slight deviations of the phase of this transfer function from 0$^{\circ}$ indicate drifts in the phase of the linear transfer functions between witness channels and $h(t)$ that are not captured by the \texttt{gstlal} calibration pipeline.
    \label{fig:noiseSubTF}}
\end{figure}
As mentioned previously, the transfer functions relating the witness channel data to $h(t)$ are known to vary over time.  This can be shown by making numerous plots of each transfer function at different times, but here we wish to use a diagnostic based on final results and requiring only one plot.  Fig.~\ref{fig:noiseSubTF} shows the transfer function $\tilde{h}_{\rm clean}(f) / \tilde{h}(f)$ computed from $\sim$22 hours of H1 data during O2.  Assuming consistently effective broadband noise subtraction, we expect that the magnitude of the transfer function plotted to be less than one and the phase to be consistent with zero.
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/H1_deltal_over_pcal_1186099217-77700.pdf}
    \caption[Time series of $\widetilde{\Delta L}(f) / \tilde{x}_{\rm pc}(f)$ at Pcal lines for $h(t)$ with noise subtraction]{
    A time series over 22 hours of H1 data during O2 of the ratio $\widetilde{\Delta L}(f) / \tilde{x}_{\rm pc}(f)$ at the two Pcal lines in the frequency band impacted by broadband noise subtraction.  Each ratio was taken using 20 s of demodulated data.  The average values of the ratios are unaffected, but noisy fluctuations are reduced, especially in the Pcal line at $f_2$ = 331.9 Hz.
    \label{fig:noiseSubCalLines}}
\end{figure}
At times when the phases of the transfer functions between $w_n$ and $h(t)$ drift, the phase of the plotted transfer function deviates from zero.  This effect is seen to a small degree below $\sim$15 Hz, indicating that the transfer functions are changing at low frequencies.  However, the variations are not significant enough to cause the noise subtraction to add noise to $h(t)$.

\subsection{Impact on calibration accuracy and uncertainty} \label{ssec:noiseSubCalAccuracy}
Although noise subtraction impacts the calibration of the noise we attempt to remove, it is not expected to impact the accuracy of the calibration of GW signals, since the witness sensors used are insensitive to GWs.  To test the impact of broadband noise subtraction on calibration accuracy, the Pcal lines can be left unperturbed in the $h(t)$ spectrum while broadband noise is subtracted.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/H1GDS_latency_1236200687-28431.pdf}
    \caption[Time series of calibration latency with and without noise subtraction]{
    A time series over 8 hours of H1 data showing the latency of the calibration pipeline with and without noise subtraction.  The latencies were computed by measuring the difference in the time that calibrated data exited the pipeline and the time that raw data with the same timestamp entered the pipeline.  The median latency $\mu_{1/2}$ and standard deviation $\sigma$ are shown for each data set in the legend.  An anomalous bimodal distribution of latencies and an associated decrease in the median latency is seen when using the noise subtraction.  The bar at the bottom indicates times when the detector was in a nominal low-noise state (green) and when it was not (red).
    \label{fig:noiseSubLatency}}
\end{figure}
Then the Pcal lines, like GWs, are insensitive to the subtraction.  Fig.~\ref{fig:noiseSubCalLines} shows time series of the ratio $\widetilde{\Delta L}(f) / \tilde{x}_{\rm pc}(f)$ at the two Pcal line frequencies at which the noise subtraction has an impact. $\sim$22 hours of uncleaned and cleaned H1 strain data were used.  Each ratio was taken using 20 s of demodulated data.
This integration time is short enough to ensure that the contribution of detector noise is large compared to that of the noise in the time-dependent calibration model, which is correlated over the 128-second length of the running median used for the TDCFs.  As expected, the data shows no significant change in the average values of the demodulated ratios caused by the noise subtraction.  However, noisy fluctuations in the ratio decreases significantly in the Pcal line at $f_2$ = 331.9 Hz, where the noise subtraction has a large impact.

\subsection{Impact on calibration latency} \label{ssec:noiseSubCalLatency}

Since the noise subtraction in the \texttt{gstlal} calibration pipeline is done in parallel to the calibration of $h(t)$ data and the filters used for noise subtraction are shorter than the calibration filters, adding noise subtraction to the calibration pipeline's procedure is not expected to significantly increase calibration latency.  Moreover, the noise subtraction uses numerous threads when running, allowing the computational cost to be divided between many CPUs, reducing any risk of a buildup of latency due to computational cost.  Fig.~\ref{fig:noiseSubLatency} shows a time series comparison of calibration latency with and without noise subtraction.  The latencies were computed by measuring the difference in the time that calibrated data exited the pipeline and the time that raw data with the same timestamp entered the pipeline.  The Hanford computing cluster was used for this test, since such tests could interfere with $h(t)$ production if done on the DMT machines.  Recent latencies seen in the production pipelines on the DMTs are consistent with these results.  An anomalous bimodal distribution of latency and an associated decrease in median latency is seen when using the noise subtraction.  The cause of this is unknown, but the median latency of the cleaned strain data is more consistent with what is expected based on the length of the calibration filters.


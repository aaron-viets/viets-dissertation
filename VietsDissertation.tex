\documentclass[12pt,notitlepage]{report}

\usepackage{graphicx,vmargin,fancyhdr} 
%I'm pretty sure vmargin and fancyhdr are required. Not sure about graphicx, but you'll probably want it anyway.

\usepackage{subcaption}
\usepackage[usenames, dvipsnames, svgnames, table]{xcolor}
\usepackage{tabto}
\newcommand\norm[1]{\left\lVert#1\right\rVert}
\usepackage{upgreek}
\usepackage{bm}

\usepackage{uwmthesis2} 
%This is required, and you will need the uwmthesis2.sty file.

\usepackage{amsmath,amssymb} 
%These are optional, but very useful.

\usepackage{siunitx}
% For using SI units

\usepackage[sort&compress]{natbib} 
%You're going to want *some* kind of bibliography package, probably either natbib or bibtex. The 'sort&compress' option affects how multiple citations are handled; if you cite your fifth, first, second, and third, references, for example, it will output [1-3,5]. 

\bibpunct{[}{]}{,}{n}{}{,} 
%This line affects what citations look like. The square brackets as the first two inputs cause references to appear within square brackets, the first comma causes multiple citations to be separated by commas, and the 'n' causes references to be numerical (as opposed to , for example, author-year formats). You are free to change this however you want; the Graduate School won't care.

\usepackage{subfiles} 
%This makes it so that different chapters can be separate files. Very useful, unless you want you main dissertation file to become a giant mess.

\usepackage{epigraph} 
%This allows you to have the optional quotes at the beginning of the Acknowledgements and each chapter.

\usepackage{array,longtable} 
%These are useful if your dissertation uses tables. The CV_MWE file uses tables.

\usepackage[hidelinks]{hyperref} 
%This is probably the most optional package here. It creates hyperlinks for the Table of Contents, citations, and equation references, which is useful whenever you want to skip to certain sections of the dissertation while reading the pdf.


%------------Set Margin{left}{top}{right}{bottom}{foot notes}{headers etc}{}----
\setmarginsrb{1.0in}{1in}{1in}{1.0in}{0pt}{0.in}{0pt}{1cm}

%------------Set page numbering style---------------
\pagestyle{fancy} \lhead{} \chead{} \rhead{} \lfoot{} \cfoot{\thepage} \rfoot{}

%%%%% number equations by section %%%%%%%%
\makeatletter
\@addtoreset{equation}{section}
\makeatother
\renewcommand{\theequation}{\thesection.\arabic{equation}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Macros for Setting Spacing Between Lines
\newcommand{\sglspc}{\setstretch{1.1}}
\newcommand{\dblspc}{\setstretch{1.6}}


% You may have a dedication and/or a copyright page if you want. The next lines control whether you do.
%\havededicationfalse
\havededicationtrue
\copyrightfalse
%\copyrighttrue

%This controls whether the format is for a doctoral dissertation or a master's thesis.
%\doctoratefalse
\doctoratetrue

% If your dissertation inlcudes figures and/or tables, you need pages listing them. The next lines control whether these pages exist.
%\figurespagefalse
\figurespagetrue
%\tablespagefalse
\tablespagetrue

%Macros that set certain values that are used in several places e.g. title, author, etc.
\newcommand{\titletext}{OPTIMIZING ADVANCED LIGO'S SCIENTIFIC OUTPUT WITH FAST, ACCURATE, CLEAN CALIBRATION}
\newcommand{\authortext}{Aaron D. Viets}
\newcommand{\mjrprof}{Jolien Creighton}
\newcommand{\submitmonth}{May}
\newcommand{\submityear}{2019}

\begin{document}
	
	\title{\titletext}
	\author{\large \authortext}
	\majorprof{\mjrprof}
	\submitdate{\submitmonth\ \submityear}
	\degree{Doctor of Philosophy}
	\program{Physics}
	\copyrightyear{\submityear}
	\majordept{Physics}
	\dedication{I dedicate this dissertation to my dear wife, Mary.} %This does nothing if you set \havededicationfalse.
	
	% The manuscript title page
	\pagestyle{uwmheadings}
	\manuscriptp
	
	\pagenumbering{roman}
	\setcounter{page}{1}
	\pagestyle{plain}
	
	\setcounter{page}{2}
	
	%---------------------------------Abstract-----------------------------------------
	\begin{center} 
		\sglspc
		
		{\Large \bf ABSTRACT}\\
		\titletext	\\
		
		\
		by \\
		{\large \authortext} \\
		\
		
		The University of Wisconsin--Milwaukee, \submitmonth\ \submityear \\
		Under the Supervision of Professor \mjrprof
	\end{center}
	\ \\
	\dblspc %set double spacing
	\vskip -1cm
	Since 2015, the direct observation of gravitational waves has opened a new window to observe the universe and made strong-field tests of Einstein's general theory of relativity possible for the first time.  During the first two observing runs of the Advanced gravitational-wave detector network, the Laser Interferometer Gravitational-wave Observatory (LIGO) and the Virgo detector have made 10 detections of binary black hole mergers and one detection of a binary neutron star merger with a coincident gamma-ray burst \cite{catalogPaper}.  This dissertation discusses methods used in low and high latency to produce Advanced LIGO's calibrated strain data, highlighting improvements to accuracy, latency, and noise reduction that have been made since the beginning of the second observing run (O2).
Systematic errors in the calibration during O2 varied by frequency, but were generally no greater that 5\% in amplitude and 3$^{\circ}$ in phase from 20 Hz to 1 kHz \cite{Craig}.  Due in part to this work, it is now possible to achieve calibration accuracy at the level of $\sim$1\% in amplitude and $\sim$1$^{\circ}$ in phase, offering improvements to downstream astrophysical analyses.  Since the beginning of O2, latency intrinsic to the calibration procedure has decreased from $\sim$12 s to $\sim$3 s.  As latency in data distribution and the sending of automated alerts to astronomers is minimized, reduction in calibration latency will become important for follow-up of events like the binary neutron star merger GW170817.  A method of removing spectral lines and broadband noise in the calibration procedure has been developed since O2, offering increases in total detectable volume during future observing runs.  High-latency subtraction of lines and broadband noise had a large impact on astrophysical analyses during O2 \cite{Derek}.  A similar data product can now be made available in low latency for the first time.
	
	\endabstract 
	\newpage
	
	% The OPTIONAL copyright page
	\ifcopyright\copyrightpage\fi
	% The OPTIONAL dedication page
	\ifhavededication\dedicationpage\fi
	% The Table of Contents
	\newpage
	\renewcommand\contentsname{\begin{center}  \vspace{-2.5cm}{\Large TABLE \ OF \ CONTENTS}\vspace{-1.5cm} \end{center}}	
	\renewcommand\listfigurename{\vspace{-2.5cm}{\begin{center}{\Large LIST \ OF \ FIGURES} \end{center}} \vspace{-0.5cm}}
	\renewcommand\listtablename{\begin{center} \vspace{-2.5cm}{\Large LIST \ OF \ TABLES}\vspace{-0.5cm} \end{center}}
	
	
	\tableofcontents
	
	\afterpreface
	
	\newpage

	%---------------------------------Preface-----------------------------------------
	\begin{center}
		{\Large \bf CONVENTIONS}
	\end{center}
	\ \\

	\begin{itemize}
		\item Greek letters ($\alpha, \beta, \gamma, ...$) indicate spacetime indices ${0,1,2,3}$, and Latin letters ($a,b,c, ...$) indicate spatial indices ${1,2,3}$.
		\item In the context of differential geometry (i.e., in Chapter 1), the Einstein summation convention is used, where there is an implied sum over repeated indices.  For example, $\eta_{\mu \nu} dx^{\mu} dx^{\nu} = \sum_{\mu=0}^3 \sum_{\nu=0}^3 \eta_{\mu \nu} dx^{\mu} dx^{\nu}$.
		\item Fourier transforms are indicated using tildes above functions, e.g. $\tilde{A}(f)$.
		\item The Fourier transform and inverse Fourier transform conventions used are
			\begin{eqnarray*}
			\tilde x(f) &=& \int_{-\infty}^{\infty} x(t) e^{-2\pi i f t} dt \\
			x(t) &=& \int_{-\infty}^{\infty} \tilde x(f) e^{2\pi i f t} df \ .
			\end{eqnarray*}
		\item Variables in bold font indicate vectors or matrices, such as $\mathbf{M}$.
	\end{itemize}

	%\dblspc%set double spacing 
	\newpage

	%---------------------------------Acronyms-----------------------------------------
	\begin{center}
		{\Large \bf ACRONYMS}
	\end{center}
	\ \\

	\noindent BS \tabto{3.5cm} Beamsplitter \\
	CARM \tabto{3.5cm} Common Arm \\
	DARM \tabto{3.5cm} Differential Arm \\
	DC \tabto{3.5cm} Direct Current \\
	DCS \tabto{3.5cm} Data and Computing Systems \\
	DMT \tabto{3.5cm} Data Monitoring Tool \\
	ETMX \tabto{3.5cm} End Test Mass X \\
	ETMY \tabto{3.5cm} End Test Mass Y \\
	FIR \tabto{3.5cm} Finite Impulse Response \\
	GDS \tabto{3.5cm} Global Diagnostic System \\
	GLib \tabto{3.5cm} GNU Library \\
	GNU \tabto{3.5cm} GNU's Not UNIX! \\
	GstLAL \tabto{3.5cm} GStreamer LIGO Algorithm Library \\
	GW \tabto{3.5cm} Gravitational Wave \\
	H1 \tabto{3.5cm} LIGO detector at Hanford, WA \\
	IIR \tabto{3.5cm} Infinite Impulse Response \\
	ITM \tabto{3.5cm} Input Test Mass \\
	ITMX \tabto{3.5cm} Input Test Mass X \\
	ITMY \tabto{3.5cm} Input Test Mass Y \\
	L1 \tabto{3.5cm} LIGO detector at Livingston, LA \\
	LAL \tabto{3.5cm} LIGO Algorithm Library \\
	Laser \tabto{3.5cm} Light Amplification by Stimulated Emisssion of Radiation \\
	LIGO \tabto{3.5cm} Laser Interferometer Gravitational-wave Observatory \\
	O1 \tabto{3.5cm} First Observing Run \\
	O2 \tabto{3.5cm} Second Observing Run \\
	O3 \tabto{3.5cm} Third Observing Run \\
	P \tabto{3.5cm} Penultimate \\
	Pcal \tabto{3.5cm} Photon calibrator \\
	PD \tabto{3.5cm} Photo Detector \\
	PRM \tabto{3.5cm} Power Recycling Mirror \\
	SI \tabto{3.5cm} International System of Units \\
	SNR \tabto{3.5cm} Signal-to-Noise Ratio \\
	SRC \tabto{3.5cm} Signal Recycling Cavity \\
	SRM \tabto{3.5cm} Signal Recycling Mirror \\
	T \tabto{3.5cm} Test \\
	TDCFs \tabto{3.5cm} Time-dependent Correction Factors \\
	TT \tabto{3.5cm} Transverse Traceless \\
	U \tabto{3.5cm} Upper Intermediate
	
	\dblspc%set double spacing
	\newpage
	
	%---------------------------------Acknowledgments-----------------------------------------
	\begin{center}
		{\Large \bf ACKNOWLEDGMENTS}
	\end{center}
	
	
	\singlespace
	%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	
	\dblspc
	Through my years in graduate school, many friends and family members have provided help and support in numerous ways, and it would not have been possible to complete this work without the help of others.  First, I would like to thank my dear wife, Mary.  She has provided me with the support and encouragement I need in many ways, and she has been there for me through all my many challenges.  Mary has been my source of motivation through all the hard work of the last several years.  I would also like to express my gratitude for the gift of our newborn son, David, who has brought so much joy into my life.  I am grateful to my parents, Alan and Amy Viets, for supporting and encouraging me throughout my life, and for fostering my fascination with science from a young age.  I would like to thank my sister Mary and my brother Toby for their constant support, encouragement, and friendship.

	I have received the gift of an excellent advisor, Jolien Creighton, who has patiently directed me and helped me to become a creative research scientist.  I would like to thank him for his constant support and his willingness to help me with problems on a moment's notice.  I would also like to thank Xavi Siemens for helping to provide me with excellent research opportunities in LIGO calibration, and for many useful discussions.

	I am especially grateful for my research collaborator and friend Maddie Wade, who has helped to train me as an expert in LIGO calibration and has always been able to offer useful advice about graduate school, teaching, and research.

	I would like to thank my fellow graduate students at UWM, especially Casey McGrath, Adil Amin, and Deep Chatterjee, for making graduate school more enjoyable.  I would like to thank Xiaoshu Liu for working with me on a collaborative project, and for his important contribution to this work.  I would like the thank Caitlin Rose for her many helpful contributions to the testing of calibration software.

	I am blessed to have many wonderful colleagues and research collaborators to work with, especially Alex Urban, who has worked closely with me on numerous occasions and always offered helpful advice and assistance.  I would also like to thank Patrick Brockill, Jeff Kissel, Joe Betzwieser, Shivaraj Kandhasamy, Evan Goetz, Alan Weinstein, Rick Savage, Sudarshan Karki, Darkhan Tuyenbayev, Les Wade, John Zweizig, Kipp Cannon, Chad Hanna, Paul Strycker, and Andrew Ashenden for many useful discussions and contributions.

	I would like to thank my dissertation committee, Jolien Crieghton, Xavi Siemens, Patrick Brady, Alan Wiseman, and Maddie Wade, for their thoughtful insights and encouragement, and for helping me to develop as a physicist and a researcher.

	The work discussed in this dissertation was supported by National Science Foundation grants PHY-1607585, PHY-1506360, and PHY-1841480.

	\newpage
	
	\pagenumbering{arabic}
	%
	\pagestyle{uwmheadings}
	
	\pagestyle{plain} \lhead{} \chead{} \rhead{} \lfoot{} \cfoot{\thepage} \rfoot{}
		
	
	\Chapter{Introduction}
	\singlespace
	%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	\dblspc
	
	\subfile{./Introduction.tex}
	\label{ch:Intro}
	
	% ---------------Chapter 2--------------- %
	\Chapter{Advanced LIGO calibration overview}
	\singlespace
	%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	\dblspc
	
	\subfile{./CalibrationOverview.tex}
	\label{ch:CalibrationOverview}
	
	% ---------------Chapter 3--------------- %
	\Chapter{The \texttt{gstlal} calibration pipeline}
	\singlespace
	%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	\dblspc

	\subfile{./GstlalCalibration.tex}
	\label{ch:GstlalCalibration}

	% ---------------Chapter 4--------------- %
	\Chapter{Designing digital filters for calibration}
	\singlespace
	%%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	\dblspc

	\subfile{./CalibrationFilters.tex}
	\label{ch:CalibrationFilters}

	% ---------------Chapter 5--------------- %
	\Chapter{Temporal variations in calibration-model parameters}
	\singlespace
	%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	\dblspc

	\subfile{./TDCFs.tex}
	\label{ch:TDCFs}

	% ---------------Chapter 6--------------- %
	\Chapter{Subtraction of excess noise from the calibrated signal}
	\singlespace
	%\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
	\dblspc

	\subfile{./NoiseSubtraction.tex}
	\label{ch:NoiseSubtraction}

	% ---------------Chapter 7--------------- %
        \Chapter{Impact of calibration accuracy and noise subtraction on astrophysical analyses}
        \singlespace
        %\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
        \dblspc

        \subfile{./Astrophysics.tex}
        \label{ch:Astrophysics}

	% ---------------Chapter 8--------------- %
        \Chapter{Conclusion}
        \singlespace
        %\epigraph{``\textit{Quote}"}{--- \textup{Quoted Person}, \textit{Quoted Work}}
        \dblspc

        \subfile{./Conclusion.tex}
        \label{ch:Conclusion}

	% ---------------Bibliography--------------- %
	\newpage
	\addcontentsline{toc}{chapter}{Bibliography}
	\bibliographystyle{h-physrev} %This sets the bibliography style to match that of the Physical Review journals, and requires the h-physrev.bst file. If you want the Bibliography to have a different style, you will need to change this line and get the relevant .bst file if it doesn't come with natbib by default.
	\bibliography{VietsDissertation} 
	
	% ---------------CV--------------- %
	\newpage
	\sglspc
	\addcontentsline{toc}{chapter}{Curriculum Vitae}
	\begin{center}
		{\bf \Large{CURRICULUM VITAE}}\\
	\end{center}
	
	\subfile{./VietsDissertationCV.tex}

\end{document}

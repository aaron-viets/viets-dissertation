\documentclass[VietsDissertation.tex]{subfiles}

\section{A study of GW170814 - methods}
Estimation of source parameters of GW events associated with compact binary coalescences is impacted by both systematic errors in the calibration and noise in the detectors.  To assess the impact on parameter estimation caused by the improvements in calibration accuracy discussed in Chapter~\ref{ch:TDCFs} and the subtraction of spectral lines and broadband noise discussed in Chapter~\ref{ch:NoiseSubtraction}, a brief study was done using 8192 s of data around the binary black hole merger GW170814.  Four different sets of calibrated strain data were produced for this study, with:
\begin{enumerate}
\item No compensation for any time dependence.  This calibration simply relies on the static reference model, produced months earlier.
\item Compensation for the time dependence of the scalar correction factors $\kappa_{\rm T}$, $\kappa_{\rm PU}$, and $\kappa_{\rm C}$.  The low-latency calibration during O2 was produced with these corrections.
\item Compensation for the time dependence of the scalar correction factors and the coupled cavity pole $f_{\rm cc}$.  A high-latency calibrated strain data set was produced shortly after O2 similar to this, called the C02 calibration.
\item Compensation for all known time dependence, as discussed in Chapter~\ref{ch:TDCFs}.  This is the first production of such an accurate calibration for the time around GW170814.
\end{enumerate}
Each of these calibrated data sets contains both strain data without noise subtracted and strain data with noise subtracted.  These four data sets were compared to the noise-subtracted high-latency strain data set produced shortly after O2, the ``cleaned C02" data.  The cleaned C02 data was produced by compensating for time dependence in the scalar correction factors and the coupled cavity pole $f_{\rm cc}$, similar to the third data set in the above list.  A high-latency noise subtraction was done after the production of the high latency $h(t)$ data to produce the cleaned C02 data.  Each of these strain data sets were processed using parameter estimation software provided by \texttt{LAL} \cite{LAL} to produce the figures shown in the following sections.

\section{Time dependence and parameter estimation}

\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{No corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_nocorr_clean.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{Scalar corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_scalarcorr_clean.pdf}
    \end{subfigure}
    \vadjust{\vskip 5mm \vskip 0pt}
    \\
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{Scalar + $f_{\rm cc}$ corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_scalarfcccorr_clean.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{All corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_allcorr_clean.pdf}
    \end{subfigure}
    \caption[Skymaps for GW170814 with varying calibration accuracy]{
    Skymaps produced by \texttt{LAL} parameter estimation software showing the estimated location of the binary black hole merger GW170814, comparing four different calibration configurations.  The upper left plot was produced without compensating for any time dependence.  The upper right plot was produced by compensating for the scalar factors $\kappa_{\rm T}$, $\kappa_{\rm PU}$, and $\kappa_{\rm C}$.  The lower left plot was produced by additionally compensating for the time dependence of the coupled cavity pole $f_{\rm cc}$.  The lower right plot was produced by compensating for all known time dependence.  Spectral lines and broadband noise were subtracted from all $h(t)$ data, in order to better resolve differences.
    \label{fig:170814skymaps}}
\end{figure}
In general, the results presented here show that compensating for time dependence in the calibration has a small impact on the estimatio
n of source parameters of a single event.  Fig.~\ref{fig:170814skymaps} shows four skymaps with the estimated locations of GW170814 for each of the four calibration configurations produced in this study.  Differences between each are distinguishable by eye.  Probability distributions and cumulative distributions of the luminosity distance $d_{\rm L}$ are shown in Fig.~\ref{fig:170814dL}, additionally including the cleaned C02 data.  Interestingly, the calibration produced that includes compensation for all time dependence is in closer agreement with the cleaned C02 data than that which compensated for time dependence in scalar factors and $f_{\rm cc}$ as was done to produce the C02 data.  This may be due to small differences in the subtraction of broadband noise, as is seen in Fig.~\ref{fig:noiseSubASDs}.  Fig.~\ref{fig:170814mchirp} shows probability distributions and cumulative distributions of the chirp mass $\mathcal{M}$ of the system.  Chirp mass is defined by
\begin{equation}
    \mathcal{M} = \frac{(m_1 m_2)^{3/5}}{(m_1 + m_2)^{1/5}} \, ,
\end{equation}
where $m_1$ and $m_2$ are the component masses of each object in a binary system.  The significance of chirp mass is that it is the only combination of masses that appears in both the amplitude and phase of the lowest order term in the post-Newtonian expansion of a gravitational waveform produced by a compact binary system.  It is therefore possible to estimate it with better accuracy than either of the component masses.  Again, small differences are seen between different calibration configurations, but none of these differences are large compared to the uncertainty in the measurement in chirp mass.
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dL_170814.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dL_170814_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and luminosity distance for GW170814]{
    Probability distributions and cumulative distributions of luminosity distance $d_{\rm L}$ for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814dL}}
\end{figure}
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/mchirp_170814.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/mchirp_170814_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and chirp mass for GW170814]{
    Probability distributions and cumulative distributions of chirp mass $\mathcal{M}$ for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814mchirp}}
\end{figure}
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/eta_170814.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/eta_170814_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and symmetric mass ratio for GW170814]{
    Probability distributions and cumulative distributions of symmetric mass ratio $\eta$ for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814eta}}
\end{figure}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/m2m1_170814.png}
    \caption[Probability distributions of $m_1$ and $m_2$ for GW170814]{
    Probability distributions in $m_1$-$m_2$ parameter space for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814m1m2}}
\end{figure}
Fig.\ref{fig:170814eta} shows similar plots for the symmetric mass ratio $\eta$, which is defined by
\begin{equation}
    \eta = \frac{m_1 m_2}{(m_1 + m_2)^2} \, .
\end{equation}
Since the symmetric mass ratio does not appear in the lowest order term in the waveform, it is generally more difficult to measure than the chirp mass, but accurate measurements of both $\eta$ and $\mathcal{M}$ would allow for accurate estimates of the component masses.  $\eta$ can take on values from 0 to 1/4, with 1/4 corresponding to equal-mass binary systems, and values near 0 corresponding to an extreme mass ratio.  Fig.~\ref{fig:170814m1m2} is a related plot showing a probability distribution in $m_1$-$m_2$ parameter space.  Results are shown for maximum-liklihood values of matched filter signal-to-noise ratio (SNR) and the log-liklihood $\ln{\mathcal{L}}$ (natural logarithm of the liklihood that the observed signal is an artifact of noise), and maximum-liklihood values and standard deviations of $\mathcal{M}$, $\eta$, and $d_{\rm L}$ in Table~\ref{tab:170814PE}.  Subtraction of spectral lines and broadband noise was used for all data sets except for one, labeled ``All\_noclean."
\begin{table}[h!]
\centering
\caption[Calibration accuracy and parameter estimation for GW170814]{\label{tab:170814PE} Estimated source parameters for GW event 170814 for several different calibration configurations}
\bigskip
\resizebox{\textwidth}{!}{%
\begin{tabular}{|| c | c | c | c | c | c | c | c | c ||}
\hline
{Corrections}  & {SNR} & {$\ln{\mathcal{L}}$} & {$\mathcal{M}$ (${\rm M_{\odot}}$)} & {$\sigma_{\mathcal{M}}$ (${\rm M_{\odot}}$)} & {$\eta$} & {$\sigma_{\eta}$} & {$d_{\rm L}$ (Mpc)} & {$\sigma_{dL}$} \\
\hline
\hline
None & 17.761014511 & -7987.96631484 & 27.0178380014 & 0.766983739453 & 0.249505657982 & 0.00597807478077 & 549.509945677 & 98.9667169454 \\
Scalars & 17.8929386653 & -7730.15944749 & 27.5261186162 & 0.823990906114 & 0.249601947665 & 0.00634804874621 & 452.426474858 & 113.179648551 \\
Scalars + $f_{\rm cc}$ & 17.7241374429 & -7982.11994613 & 27.4872475722 & 0.712435525191 & 0.249515106256 & 0.00573912784603 & 531.156693048 & 112.137674556 \\
All & 17.6728626104 & -7992.78217884 & 26.4807135107 & 0.80906604071 & 0.249047391514 & 0.00491675806994 & 625.835533585 & 126.148960787 \\
All\_noclean & 15.9283148007 & -7979.50357234 & 27.8498859539 & 0.949267529509 & 0.249875742544 & 0.00825849765094 & 310.151922918 & 154.781324672 \\
C02\_clean & 17.5107799703 & -7978.21959427 & 27.6783242919 & 0.789370567018 & 0.248035089058 & 0.00498150356785 & 600.889444137 & 124.124795642 \\
\hline
\end{tabular}}
\end{table}

\section{Noise subtraction and parameter estimation}

\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/skymap_170814_allcorr.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/skymap_170814_allcorr_clean.pdf}
    \end{subfigure}
    \caption[Skymaps for GW170814 with and without noise subtraction]{
    Skymaps produced by \texttt{LAL} parameter estimation software showing the estimated location of the binary black hole merger GW170814, with and without subtraction of spectral lines and broadband noise.  All known time dependence was compensated for in both plots.
    \label{fig:170814skymapsNoiseSub}}
\end{figure}





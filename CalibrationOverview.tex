\documentclass[VietsDissertation.tex]{subfiles}

The goal of Advanced LIGO's detectors is to measure GW signals and record them as a time series.  However, the raw output of the detectors is not the GW strain; it a digitized double-precision error signal called $d_{\rm err}$ sampled at 16384 Hz, in arbitrary units called counts, that represents the intensity of the laser light at the GW readout port as measured by a photodiode.  Moreover, the relationship of the error signal to the GW strain is nontrivial, with dependence on both frequency and time.

Calibration of Advanced LIGO data entails reconstructing the strain signal in the detectors.  GWs induce changes in the differential arm (DARM) length of the detectors:
\begin{equation} \label{eq:DeltaL}
    \Delta L_{\rm free}(t) = \Delta L_{\rm x}(t) - \Delta L_{\rm y}(t) \, ,
\end{equation}
where $\Delta L_{\rm x}$ and $\Delta L_{\rm y}$ are changes in the length of the X-arm and the Y-arm, respectively.  Despite the use of active seismic isolation and a quadrupole pendulum system to supress low-frequency seismic noise, the detectors require additional mitigation of noise to achieve a stable low-noise state.  This is achieved using feedback control through an actuation system which removes a controlled DARM length $\Delta L_{\rm ctrl}$ from $\Delta L_{\rm free}$, to produce a residual DARM length:
\begin{equation}
    \Delta L_{\rm res} = \Delta L_{\rm free} - \Delta L_{\rm ctrl} \, .
\end{equation}
The final calibration product is the dimensionless strain signal, defined as
\begin{equation} \label{eq:hoftDefinition}
    h(t) = \frac{\Delta L_{\rm free}(t)}{L} \, ,
\end{equation}
where $L = (L_{\rm x} + L_{\rm y}) / 2$ is the average length of the arms.

\section{Black box models of the detector's response to gravitational waves} \label{sec:BBmodels}

\begin{figure}
	\includegraphics[width=\textwidth]{figures/DARM_loop_verbose}
	\caption[Advanced LIGO's DARM feedback control loop]{\label{fig:DARM_loop} A block diagram of the Advanced LIGO differential arm (DARM) feedback control loop.  Noise and GW signals enter at the upper left as $\Delta L_{\rm free}$.  The sensing function $C$ represents the conversion from meters of residual DARM displacement $\Delta L_{\rm res}$ to counts in the error signal $d_{\rm err}$.  The digital filter $D$ is used to produce the control signal $d_{\rm ctrl}$, which is sent to the actuation function $A$.  The actuation function converts the control signal to a controlled DARM displacement $\Delta L_{\rm ctrl}$, which is removed from $\Delta L_{\rm free}$ to produce $\Delta L_{\rm res}$.  The actuation function is split into three stages $A_j$ ($j \in \{T, P, U\}$), corresponding to the three stages of the suspensions system used to control DARM motion.  The filter functions $F_j$ include all filters (e.g., lock filters) that occur before injections in each stage, and the functions $A_{i,0}$ are the remaining portions of the $A_j$.  The injections $x_{\rm pc}$, made using the photon calibrator (Pcal), and $x_i$, made in each stage of the actuation, are used to measure the calibration at select frequencies.  The injection $x_{\rm ctrl}$ was used during O2 instead of $x_{\rm P}$ and $x_{\rm U}$.}
\end{figure}
Changes in DARM cause fluctuations in the intensity of the laser light measured by the photodiode at the GW readout port, which is converted to the digital error signal $d_{\rm err}$.  The transfer function relating the error signal to residual DARM length changes is called the sensing function $C$ and is defined by
\begin{equation} \label{eq:Cdef}
    \tilde{d}_{\rm err}(f) = \tilde{C}(f) \widetilde{\Delta L}_{\rm res}(f) \, ,
\end{equation}
where the tilde denotes a Fourier transform.  The error signal is digitally filtered to produce a control signal $d_{\rm ctrl}$:
\begin{equation} \label{eq:darmCtrl}
    \tilde{d}_{\rm ctrl}(f) = \tilde{D}(f) \tilde{d}_{\rm err}(f) \, ,
\end{equation}
where the digital filter $\tilde{D}$ acts primarily as a low-pass filter, but may also contain notches to remove resonant frequencies of the actuation such as violin modes.  The control signal is then sent to the actuation system to remove excess low-frequency noise:
\begin{equation} \label{eq:Adef}
    \widetilde{\Delta L}_{\rm ctrl}(f) = \tilde{A}(f) \tilde{d}_{\rm ctrl}(f) \, .
\end{equation}
This describes a feedback control loop called the DARM loop that is used to mitigate low-frequency noise, depicted in Fig.~\ref{fig:DARM_loop}.  The DARM loop can be used to solve for $\Delta L_{\rm free}$ in terms of the error signal, the result being
\begin{equation} \label{eq:Rdef}
    \widetilde{\Delta L}_{\rm free}(f) = \tilde{R}(f) \tilde{d}_{\rm err}(f) \, ,
\end{equation}
where $R$ is the response function\footnote{Note that $R$ is not the response function of the detector, but the inverse, that is, the intended response function of the calibration.} given by
\begin{equation} \label{eq:R}
    \tilde{R}(f) = \frac{1 + \tilde{A}(f)\tilde{D}(f)\tilde{C}(f)}{\tilde{C}(f)} \, .
\end{equation}
However, this is not the method used by Advanced LIGO's calibration pipelines to compute $h(t)$.  The solution used to compute $h(t)$ uses both the error and control signals:
\begin{equation} \label{eq:hoft}
    \widetilde{\Delta L}_{\rm free}(f) = \tilde{C}^{-1}(f) \tilde{d}_{\rm err}(f) + \tilde{A}(f) \tilde{d}_{\rm ctrl}(f) \, .
\end{equation}
The motivation behind using this method is to avoid using a calibration model that depends on the digital filter $D$, due to the fact that $D$ can be changed for detector commissioning purposes and is not continuously measured in the calibration process.  The methods discussed later in Chapter~\ref{ch:TDCFs} to continuously track temporal variations in the calibration models do, however, have dependence on $D$, nullifying the benefit.  This is an area of current development.

Note that the sensing function $C$ is a linear transfer function, and can therefore accurately model the relationship between  $\Delta L_{\rm res}$ and $d_{\rm err}$ only if that relationship is in fact linear.  However, that relationship cannot be linear if the unperturbed state of the detector allows no light to exit at the GW readout port.  Moreover, if this were the case, one could not determine the sign of $\Delta L_{\rm res}$.  In order to ensure linearity, a small offset is digitally added into the DARM loop, so that in an unperturbed state, a small amount of light escapes to the GW readout port.

\section{Time-domain calibration} \label{sec:TDcalibration}
Although calibration models are best represented analytically in the frequency domain, calibration of Advanced LIGO data is done in the time domain.  This has been the case since Initial LIGO's second science run, and the development of the original time-domain calibration is described in \cite{Xavi}.  An update of the methods used for time-domain calibration in Advanced LIGO is given in \cite{hoft}.  The primary benefit of applying the calibration model in the time domain, as opposed the frequency domain, is that the filtering of time-domain data can be done with very low latency, whereas taking Fourier transforms of consecutive segments of input data to apply a frequency-domain model adds latency.

Time-domain calibration requires the construction of digital filters using the frequency-domain models for $\tilde{A}$ and $\tilde{C}^{-1}$.  Finite impulse response (FIR) filters representing the models are then convolved with input data to apply the models:
\begin{equation} \label{eq:hoftTD}
    h(t) = C^{-1} \ast d_{\rm err}(t) + A \ast d_{\rm ctrl}(t) \, .
\end{equation}
The operation
\begin{equation} \label{eq:convolution}
    F \ast g(t) = \int_{-\infty}^{\infty} F(\tau) g(t - \tau) d \tau
\end{equation}
denotes convolution of the filter $F$ with the signal $g(t)$, equivalent to a point-by-point multiplication in the frequency domain.

\section{Sensing function} \label{sec:C}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/Sensing_modelparams_L1_20190117.pdf}
    \caption[Bode plot of the sensing function model]{
    A Bode plot of the sensing function model for the L1 detector before O3.  Measurements used to produce this model were taken on 2019-01-17.
    \label{fig:C}}
\end{figure}
The full sensing function model used by the calibration pipelines is given by
\begin{equation} \label{eq:C}
    \tilde{C}(f; t) = \kappa_{\rm C}(t) \, \left(\frac{H_{\rm C}}{1 + i f / f_{\rm cc}(t)}\right) \, \left(\frac{f^2}{f^2 + f_{\rm s}^2(t) - i f f_{\rm s}(t) / Q(t)}\right) C_{\rm R}(f) \exp[-2 \pi i f \tau_{\rm C}] \, .
\end{equation}
The gain $H_{\rm C}$ represents the conversion from meters of DARM displacement to counts.  The dimensionless scalar $\kappa_{\rm C}(t)$ encodes the time-dependence of the gain $H_{\rm C}$, observed to fluctuate by $\sim$10\%.  The coupled cavity pole frequency $f_{\rm cc}(t)$ is the characteristic frequency at which the detector response is significantly attenuated due to finite average photon storage time in the Fabry P\'erot cavities.  During O3, the coupled cavity pole frequency is expected to have a value of $\sim$400 Hz.  $\tau_{\rm C}$ is a constant time delay due to light-travel time across the length of each arm and an additional time delay in acquiring the digital signal.  The factor $C_{\rm R}(f)$ encodes the remaining frequency dependence above $\sim$1 kHz due to photodiode electronics and signal-processing filters.
The second term in parenthesis represents the impact of the signal recycling cavity (SRC) on the detector response.  $f_{\rm s}(t)$ and $Q(t)$ are the resonant frequency and quality factor of the optical anti-spring of the SRC, respectively.  An optical spring (or anti-spring) exists in an optomechanical cavity if there is a linear relationship between the length of the cavity and the photon pressure on the mirrors.  The value of $f_{\rm s}$ is generally expected to remain below $\sim$10 Hz.

A Bode plot showing the sensing function model for the L1 detector before O3 is shown in Fig.~\ref{fig:C}.  The same model is used in Fig.~\ref{fig:filtersResponse}, showing how well the calibration filters implement the application of the response function $R$ to the raw data.

\section{Actuation function} \label{sec:A}
As depicted in Fig.~\ref{fig:aLIGO}, the actuation system utilizes a quadrupole pendulum systems hung parallel to the test-mass suspension system.  The lowest three stages are used to control DARM motion in one of the arms (typically the X-arm).  At the lowest stage, called the test mass (T) stage, an electrostatic actuator is used to control DARM motion.  At the penultimate (P) and upper intermediate (U) stages, electromagnetic actuators are used.  Digital filters are applied in each stage of the actuation to direct the low-frequency content to the higher stages and the high-frequency content to the lower stages.  The full actuation model is given by
\begin{align} \label{eq:A}
    \tilde{A}(f; t) = \bigl[\kappa_{\rm U}(t) &e^{2 \pi i f \tau_{\rm U}(t)} \tilde{F}_{\rm T}(f) \tilde{F}_{\rm P}(f) \tilde{F}_{\rm U}(f) \tilde{A}_{\rm U, 0}(f) \bigr. \\
        &+ \kappa_{\rm P}(t) e^{2 \pi i f \tau_{\rm P}(t)} \tilde{F}_{\rm T}(f) \tilde{F}_{\rm P}(f) \tilde{A}_{\rm P, 0}(f) \nonumber \\
        &+ \bigl. \kappa_{\rm T}(t) e^{2 \pi i f \tau_{\rm T}(t)} \tilde{F}_{\rm T}(f) \tilde{A}_{\rm T, 0}(f)\bigr] \exp[-2 \pi i f \tau_{\rm A}] \, , \nonumber
\end{align}
where $\tilde{A}_{j, 0}(f)$ represents the frequency response of the $j$-stage actuator for $j \in {\rm\{T, P, U\}}$.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/Actuation_modelparams_L1_20190117.pdf}
    \caption[Bode plot of the actuation function model]{
    A Bode plot of the actuation function model for the L1 detector before O3.  Measurements used to produce this model were taken on 2019-01-17.
    \label{fig:A}}
\end{figure}
$\kappa_j(t)$ encodes the time-dependence of the strength of the $j$-stage actuator, and $\tau_j(t)$ represents a variable computational time advance associated with the $j$-stage actuator, generally expected to be close to zero \cite{freqDepCorr}.  The digital filter functions $F_j(f)$ are used before each actuator to distribute the frequency content of $d_{\rm ctrl}$ to each stage.  $\tau_{\rm A}$ is a constant computational time delay.  Fig.~\ref{fig:A} shows a Bode plot of the actuation function for the L1 detector shortly before O3, with each stage of actuation shown individually.  The same model is used in Fig.~\ref{fig:filtersResponse}.

\section{Measurements} \label{sec:measurements}
Calibration of Advanced LIGO data requires a fundamental reference for absolute displacement calibration in order to take measurements of the detectors.  This is provided by a radiation pressure actuator located at each end station known as a photon calibrator (Pcal).  The Pcal is an auxiliary laser system that induces fiducial test mass displacements via radiation pressure with force coefficients derived from laser power measurements traceable to SI units.  The powers of these laser beams that reflect from the the test mass surfaces are measured using calibrated sensors, thus providing continuous absolute calibration when the interferometers are operating in their nominal configurations.  The overall 1-$\sigma$ uncertainty in the displacements induced by photon calibrators was 0.75\% during O2 \cite{Pcal} with the long-term stability of the calibrated length variations verified during year-long observing runs \cite{P1100013}.  Further reduction of the 1-$\sigma$ uncertainty in the displacements induced by the Pcals is possible and expected to occur during O3.  The Pcal signals are injected at the same location in the DARM feedback loop as free DARM motion $\Delta L_{\rm free}$, as shown in Fig.~\ref{fig:DARM_loop}.

Full measurements of the sensing and actuation functions are achieved using swept sine injections made using the Pcal and each stage of the actuation system \cite{calCompanion}.  The parameters of the analytical models for $A$ and $C$ are then fit to these measurements to inform a static reference model, denoted by $A^{\rm model}$ and $C^{\rm model}$.  All time-dependent parameters are set to their nominal values in the static reference model, i.e., $\kappa_{\rm C} \rightarrow 1, f_{\rm cc} \rightarrow f_{\rm cc}^{\rm model}, f_{\rm s} \rightarrow f_{\rm s}^{\rm model}, Q \rightarrow Q^{\rm model}, \kappa_{\rm T} \rightarrow 1, \tau_{\rm T} \rightarrow 0, \kappa_{\rm P} \rightarrow 1, \tau_{\rm P} \rightarrow 0, \kappa_{\rm U} \rightarrow 1, \tau_{\rm U} \rightarrow 0$, reducing the models for $C$ and $A$ to
\begin{align}
    \tilde{C}^{\rm model}(f) &= \left(\frac{H_{\rm C}}{1 + i f / f_{\rm cc}^{\rm model}}\right) \, \left(\frac{f^2}{f^2 + (f_{\rm s}^{\rm model})^2 - i f f_{\rm s}^{\rm model} / Q^{\rm model}}\right) C_{\rm R}(f) \exp[-2 \pi i f \tau_{\rm C}] \\
    \tilde{A}^{\rm model}(f) &= \bigl[\tilde{F}_{\rm T}(f) \tilde{F}_{\rm P}(f) \tilde{F}_{\rm U}(f) \tilde{A}_{\rm U, 0}(f) + \tilde{F}_{\rm T}(f) \tilde{F}_{\rm P}(f) \tilde{A}_{\rm P, 0}(f) \bigr. \nonumber \\
    &\qquad \qquad \qquad \qquad \qquad \qquad + \bigl. \tilde{F}_{\rm T}(f) \tilde{A}_{\rm T, 0}(f)\bigr] \exp[-2 \pi i f \tau_{\rm A}] \, .
\end{align}
Full measurements are made prior to observing runs and periodically throughout observing runs in order to produce filters for calibration purposes and inform a calibration uncertainty budget, described in \cite{Craig}.

\section{Temporal variations in the response of the detectors} \label{sec:introKappas}
As implied by Eqs.~\ref{eq:C} and ~\ref{eq:A}, the calibration models for $A$ and $C$ are known to show small temporal variations \cite{Darkhan}, captured in the model by the time-dependent correction factors (TDCFs) $\kappa_{\rm T}$, $\tau_{\rm T}$, $\kappa_{\rm P}$, $\tau_{\rm P}$, $\kappa_{\rm U}$, $\tau_{\rm U}$, $\kappa_{\rm C}$, $f_{\rm cc}$, $f_{\rm s}$, and $Q$.  In order to track and compensate for these temporal variations, sinusoidal excitations are continuously injected using a Pcal and each stage of the actuation system.  These injections add loud spectral lines to the $h(t)$ spectrum, called calibration lines.  The Pcals thus also provide stable references for continuously monitoring temporal variations in the responses of the interferometers.  If not compensated for, temporal variations can lead to systematic errors in the calibrated strain data as large as $\sim$20\% at some frequencies.  The largest variations are observed in the optical gain and the strength of the elecrostatic actuator at the test mass stage of the actuation, tracked by the parameters $\kappa_{\rm C}$ and $\kappa_{\rm T}$, respectively. Changes in the optical gain can be caused by drifts in the alignment of the mirrors or intentional changes to the input laser power, and can occur on timescales of several minutes.  The strength of the electrostatic actuator gradually increases during normal detector operation on timescales of days to weeks due to the slow accumulation of charge on the test mass.  The methods used to track and compensate for slow changes in the TDCFs are described in detail in Chapter~\ref{ch:TDCFs}.

\section{Calibration pipelines} \label{introPipelines}
Low-latency calibration of Advanced LIGO data is done in two stages.  The first, called the front-end calibration pipeline, is performed on the same computers that operate the detector's feedback control system.  The front-end calibration pipeline uses infinite impulse response (IIR) filters and is run using real-time code in the front-end system where all operations are done on \SI{61}{\micro\second} digital sampling intervals.  An advantage of the front-end calibration pipeline is being directly hooked into all of the other front-end computer systems, allowing seamless access to all of the appropriate detector models and parameters.  This enables the calibration model to remain up-to-date and in-sync with the detector.  For this reason, it is a long-term goal to perform the low-latency calibration procedure entirely in the front end.  However, several limitations to calibration accuracy in the current front-end calibration pipeline necessitate the second stage of the low-latency calibration procedure.  First, due to the real-time nature of the front-end computing system, a phase advance cannot be applied to the output, leading to several cycles of delay in the output.  Second, it is difficult using IIR filters to model arbitrary transfer functions, as is needed to apply the models of $A$ and $C^{-1}$ to the raw data.  This is especially true at high frequencies, due to poles in the calibration models above the Nyquist frequency used to model the response of analog electronics.  Finally, although it is now possible to compensate for time dependence in the front-end calibration pipeline, the TDCFs computed in the front end are subject to large noisy fluctuations.  Compensation for time dependence is expected to improve front-end calibration accuracy during O3, but due to these large fluctuations, the second stage of the low-latency calibration procedure performs its own calculation of the TDCFs to compensate for time dependence.

Due to the systematic errors currently present in the front-end calibration, its calibrated output is sent to another pipeline called the \texttt{gstlal} calibration pipeline.  The low-latency \texttt{gstlal} calibration pipeline receives as inputs the front-end outputs $\Delta L_{\rm res}$, $\Delta L_{\rm T}$, $\Delta L_{\rm P}$, and $\Delta L_{\rm U}$ and applies finite impulse response (FIR) filters to correct the known systematic errors present due to the limitations of the front end's IIR filters.  It is much easier using FIR filters to model an arbitrary transfer function, making the task of applying corrections to the front-end models for $A$ and $C^{-1}$ almost trivial.  Additionally, the FIR filters of the \texttt{gstlal} calibration pipeline are used to high-pass filter the $h(t)$ data for the benefit of downstream analysis.  The TDCFs computed in the \texttt{gstlal} calibration pipeline are accepted or rejected based on the coherence of the calibration lines and passed through a running median to avoid the large noisy fluctuations seen in the front end's values.  Although a similar method has recently been implemented in the front end, not all of these noisy fluctuations have been removed.

In high latency, the entire calibration procedure is done using the \texttt{gstlal} calibration pipeline, using as inputs $d_{\rm err}$ and $d_{\rm ctrl}$.  A high latency calibration is generally necessary due to mistakes made in the low latency calibration, and errors in or improvements to the calibration models and input data discovered after the low-latency calibration is produced.

The methods and improvements to the calibration procedure discussed in the following chapters were developed in the \texttt{gstlal} calibration pipeline.



\documentclass[VietsDissertation.tex]{subfiles}
Estimation of source parameters of GW events associated with compact binary coalescences is impacted by both systematic errors in the calibration and noise in the detectors.  To assess the impact on parameter estimation caused by the improvements in calibration accuracy discussed in Chapter~\ref{ch:TDCFs} and the subtraction of spectral lines and broadband noise discussed in Chapter~\ref{ch:NoiseSubtraction}, a brief study was done using two GW events from O2 data: the binary black hole merger GW170814 and the binary neutron star merger GW170817.

\section{Methods of the study}
For the study, 8192 s of data was calibrated around the time of each event.  Four different sets of calibrated strain data were produced, with:
\begin{enumerate}
\item No compensation for any time dependence.  This calibration simply relies on the static reference model, produced months earlier.
\item Compensation for the time dependence of the scalar correction factors $\kappa_{\rm T}$, $\kappa_{\rm PU}$, and $\kappa_{\rm C}$.  The low-latency calibration during O2 was produced with these corrections.
\item Compensation for the time dependence of the scalar correction factors and the coupled cavity pole $f_{\rm cc}$.  A high-latency calibrated strain data set was produced shortly after O2 similar to this, called the C02 calibration.
\item Compensation for all known time dependence, as discussed in Chapter~\ref{ch:TDCFs}.  This is the first production of such an accurate calibration for either of the events.
\end{enumerate}
Each of these calibrated data sets contains both strain data without noise subtracted and strain data with noise subtracted.  These four data sets were compared to the noise-subtracted high-latency strain data set produced shortly after O2, the ``cleaned C02" data.  The cleaned C02 data was produced by compensating for time dependence in the scalar correction factors and the coupled cavity pole $f_{\rm cc}$, similar to the third data set in the above list.  A high-latency noise subtraction was done after the production of the high latency $h(t)$ data to produce the cleaned C02 data.  Each of these strain data sets was analyzed using a nested sampling algorithm provided by \texttt{LAL} \cite{LAL} to estimate parameters based on the strain data from the two Advnaced LIGO detectors.  Data from the Virgo detector was not used for this study.  For the binary neutron star event GW170817, the sky localization was fixed at the location of the electromagnetic counterpart.

\section{Time dependence and parameter estimation}

\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{No corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_nocorr_clean.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{Scalar corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_scalarcorr_clean.pdf}
    \end{subfigure}
    \vadjust{\vskip 5mm \vskip 0pt}
    \\
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{Scalar + $f_{\rm cc}$ corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_scalarfcccorr_clean.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \underline{All corrections} \vadjust{\vskip 1mm \vskip 0pt} \\
        \includegraphics[width=\textwidth]{figures/skymap_170814_allcorr_clean.pdf}
    \end{subfigure}
    \caption[Skymaps for GW170814 with varying calibration accuracy]{
    Skymaps produced by \texttt{LAL} parameter estimation software showing the estimated location of the binary black hole merger GW170814, comparing four different calibration configurations.  The upper left plot was produced without compensating for any time dependence.  The upper right plot was produced by compensating for the scalar factors $\kappa_{\rm T}$, $\kappa_{\rm PU}$, and $\kappa_{\rm C}$.  The lower left plot was produced by additionally compensating for the time dependence of the coupled cavity pole $f_{\rm cc}$.  The lower right plot was produced by compensating for all known time dependence.  Spectral lines and broadband noise were subtracted from all $h(t)$ data, in order to better resolve differences.
    \label{fig:170814skymaps}}
\end{figure}
In general, the results presented here show that compensating for time dependence in the calibration has a small impact on the estimation of source parameters of a single event.

\subsection{GW170814}
Fig.~\ref{fig:170814skymaps} shows four skymaps with the estimated locations of GW170814 for each of the four calibration configurations produced in this study.  Differences between each are distinguishable by eye.  Probability distributions and cumulative distributions of the luminosity distance $d_{\rm L}$ are shown in Fig.~\ref{fig:170814dL}, additionally including the cleaned C02 data.  Interestingly, the calibration produced that includes compensation for all time dependence is in closer agreement with the cleaned C02 data than that which compensated for time dependence in scalar factors and $f_{\rm cc}$ as was done to produce the C02 data.  This may be due to small differences in the subtraction of broadband noise, as is seen in Fig.~\ref{fig:noiseSubASDs}.  Fig.~\ref{fig:170814mchirp} shows probability distributions and cumulative distributions of the chirp mass $\mathcal{M}$ of the system in the detector frame.  Chirp mass is defined by
\begin{equation}
    \mathcal{M} = \frac{(m_1 m_2)^{3/5}}{(m_1 + m_2)^{1/5}} \, ,
\end{equation}
where $m_1$ and $m_2$ are the component masses of each object in a binary system.  The significance of chirp mass is that it is the only combination of masses that appears in both the amplitude and phase of the lowest order term in the post-Newtonian expansion of a gravitational waveform produced by a compact binary system.  It is therefore possible to estimate it with better accuracy than either of the component masses.  Again, small differences are seen between different calibration configurations, but none of these differences are large compared to the uncertainty in the measurement of chirp mass.
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dL_170814.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dL_170814_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and luminosity distance for GW170814]{
    Probability distributions and cumulative distributions of luminosity distance $d_{\rm L}$ for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814dL}}
\end{figure}
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/mchirp_170814.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/mchirp_170814_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and chirp mass for GW170814]{
    Probability distributions and cumulative distributions of chirp mass $\mathcal{M}$ for GW170814 in the detector frame, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814mchirp}}
\end{figure}
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/eta_170814.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/eta_170814_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and symmetric mass ratio for GW170814]{
    Probability distributions and cumulative distributions of symmetric mass ratio $\eta$ for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814eta}}
\end{figure}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/m2m1_170814.png}
    \caption[Probability distributions of $m_1$ and $m_2$ for GW170814]{
    90\% confidence regions in $m_1$-$m_2$ parameter space (detector frame) for GW170814, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170814m1m2}}
\end{figure}
Fig.~\ref{fig:170814eta} shows similar plots for the symmetric mass ratio $\eta$, which is defined by
\begin{equation}
    \eta = \frac{m_1 m_2}{(m_1 + m_2)^2} \, .
\end{equation}
Since the symmetric mass ratio does not appear in the lowest order term in the waveform, it is more difficult to measure than the chirp mass, but accurate measurements of both $\eta$ and $\mathcal{M}$ would allow for accurate estimates of the component masses.  $\eta$ can take on values from 0 to 1/4, with 1/4 corresponding to equal-mass binary systems, and values near 0 corresponding to an extreme mass ratio.  Fig.~\ref{fig:170814m1m2} is a related plot showing the 90\% confidence region in $m_1$-$m_2$ parameter space in the detector frame.  Results are shown for average values of matched filter signal-to-noise ratio (SNR) and the log-liklihood $\ln{\mathcal{L}}$ (natural logarithm of the liklihood that the observed signal is an artifact of noise), and average values and standard deviations of $\mathcal{M}$ (detector frame), $\eta$, and $d_{\rm L}$ in Table~\ref{tab:170814PE}.  Subtraction of spectral lines and broadband noise was used for all data sets except for one, labeled ``All\_noclean."
\begin{table}[h!]
\centering
\caption[Calibration accuracy and parameter estimation for GW170814]{\label{tab:170814PE} Estimated source parameters for GW170814 for several different calibration configurations}
\bigskip
\resizebox{\textwidth}{!}{%
\begin{tabular}{|| c | c | c | c | c | c | c | c | c ||}
\hline
{Corrections} & {SNR} & {$\ln{\mathcal{L}}$} & {$\mathcal{M}$ (${\rm M_{\odot}}$)} & {$\sigma_{\mathcal{M}}$ (${\rm M_{\odot}}$)} & {$\eta$} & {$\sigma_{\eta}$} & {$d_{\rm L}$ (Mpc)} & {$\sigma_{dL}$} \\
\hline
\hline
None & 17.4921 & -7993.21 & 26.5579 & 0.7670 & 0.246079 & 0.005978 & 562.231 & 98.967 \\
Scalars & 17.6186 & -7735.51 & 26.5976 & 0.8240 & 0.245715 & 0.006348 & 538.781 & 113.180 \\
Scalars + $f_{\rm cc}$ & 17.4422 & -7987.60 & 26.7048 & 0.7124 & 0.245989 & 0.005739 & 561.622 & 112.137 \\
All & 17.4129 & -7997.77 & 26.8131 & 0.8091 & 0.246412 & 0.004917 & 596.102 & 126.149 \\
All\_noclean & 15.5780 & -7985.56 & 26.9420 & 0.9493 & 0.244196 & 0.008258 & 705.301 & 154.781 \\
C02\_clean & 17.2492 & -7983.27 & 27.0883 & 0.7894 & 0.246523 & 0.004982 & 597.148 & 124.125 \\
\hline
\end{tabular}}
\end{table}

\subsection{GW170817}
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dL_170817.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dL_170817_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and luminosity distance for GW170817]{
    Probability distributions and cumulative distributions of luminosity distance $d_{\rm L}$ for GW170817, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170817dL}}
\end{figure}
\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/mchirp_170817.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/mchirp_170817_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and chirp mass for GW170817]{
    Probability distributions and cumulative distributions of chirp mass $\mathcal{M}$ for GW170817 in the detector frame, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170817mchirp}}
\end{figure}
The L1 calibrated data from the binary neutron star inspiral GW170817 required additional processing due to a loud glitch present in the data about a second before the time of coalescence (see \cite{GW170817}).  Left unaddressed, this glitch would significantly corrupt the parameter estimation.  To remove the glitch, the calibrated data was multiplied by an inverted Planck-taper window to taper the $h(t)$ data to zero for the duration of the glitch ($\sim$0.25 s).  The duration of the taper used for the window was 0.25 s.  This differs from what was done to produce the cleaned C02 data, which is included in the following plots for comparison.  The cleaned C02 data was produced by subtracting a model of the glitch based on wavelet reconstruction from the $h(t)$ data, according to the methods described in \cite{glitchSubtraction}.

\begin{figure}
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/eta_170817.png}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/eta_170817_cum.png}
    \end{subfigure}
    \caption[Calibration accuracy and symmetric mass ratio for GW170817]{
    Probability distributions and cumulative distributions of symmetric mass ratio $\eta$ for GW170817, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170817eta}}
\end{figure}

Probability distributions and cumulative distributions of the luminosity distance $d_{\rm L}$ are seen in Fig.~\ref{fig:170817dL}, showing only small differences between calibration configurations.  Fig.~\ref{fig:170817mchirp} shows probability distributions and cumulative distributions of the chirp mass $\mathcal{M}$ of the system in the detector frame, and Fig.~\ref{fig:170817eta} shows similar plots for the symmetric mass ratio $\eta$.  Fig.~\ref{fig:170817m1m2} shows the 90\% confidence region in $m_1$-$m_2$ parameter space in the detector frame.  Results are shown for average values of matched filter SNR and the log-liklihood $\ln{\mathcal{L}}$, and average values and standard deviations of $\mathcal{M}$ (detector frame), $\eta$, and $d_{\rm L}$ in Table~\ref{tab:170817PE}.  Subtraction of spectral lines and broadband noise was used for all data sets except for one, labeled ``All\_noclean."
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/m2m1_170817.png}
    \caption[Probability distributions of $m_1$ and $m_2$ for GW170817]{
    90\% confidence regions in $m_1$-$m_2$ parameter space (detector frame) for GW170817, comparing 5 versions of calibration with: no compensation for time dependence; corrections for scalar factors; corrections for scalar factors and $f_{\rm cc}$; compensation for all known time dependence; high-latency calibration produced just after O2 (C02).  Spectral lines and broadband noise were subtracted from all data sets.
    \label{fig:170817m1m2}}
\end{figure}

\begin{table}[h!]
\centering
\caption[Calibration accuracy and parameter estimation for GW170817]{\label{tab:170817PE} Estimated source parameters for GW170817 for several different calibration configurations}
\bigskip
\resizebox{\textwidth}{!}{%
\begin{tabular}{|| c | c | c | c | c | c | c | c | c ||}
\hline
{Corrections} & {SNR} & {$\ln{\mathcal{L}}$} & {$\mathcal{M}$ (${\rm M_{\odot}}$)} & {$\sigma_{\mathcal{M}}$ (${\rm M_{\odot}}$)} & {$\eta$} & {$\sigma_{\eta}$} & {$d_{\rm L}$ (Mpc)} & {$\sigma_{dL}$} \\
\hline
\hline
None & 32.0659 & -435150 & 1.19752 & 0.00008 & 0.247647 & 0.002387 & 40.1608 & 8.5824 \\
Scalars & 31.9267 & -435096 & 1.19752 & 0.00008 & 0.247676 & 0.002343 & 40.5240 & 7.0810 \\
Scalars + $f_{\rm cc}$ & 31.8898 & -435183 & 1.19752 & 0.00008 & 0.247594 & 0.002445 & 39.9722 & 7.7153 \\
All & 31.5160 & -434784 & 1.19754 & 0.00008 & 0.247977 & 0.002161 & 40.5781 & 7.73101 \\
All\_noclean & 29.7538 & -436495 & 1.19757 & 0.00009 & 0.247963 & 0.002263 & 40.6685 & 6.9811 \\
C02\_clean & 32.3551 & -435432 & 1.19752 & 0.00008 & 0.247626 & 0.002558 & 39.5273 & 7.2086 \\
\hline
\end{tabular}}
\end{table}

In general, the impact of compensating for the time dependence of the TDCFs was relatively small for both events compared to the uncertainty in the parameters.  The impact would likely become larger in signals with higher SNR, or in results that will eventually be based on large numbers of measurements from numerous observing runs, such as the neutron star equation of state \cite{NSEOS}, estimates of rates and populations \cite{2019RatesPaper}, and measurements of the Hubble constant $H_0$ \cite{H0paper}.

\section{Noise subtraction and parameter estimation}
\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/skymap_170814_allcorr.pdf}
    \end{subfigure}
    \;
    \begin{subfigure}{0.485\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/skymap_170814_allcorr_clean.pdf}
    \end{subfigure}
    \caption[Skymaps for GW170814 with and without noise subtraction]{
    Skymaps produced by \texttt{LAL} parameter estimation software showing the estimated location of the binary black hole merger GW170814, with and without subtraction of spectral lines and broadband noise.  The cleaned data is on the right.  Due to noise subtraction, the 1-$\sigma$ confidence region is reduced in size from 462 square degrees to 310 square degrees.  All known time dependence was compensated for in both plots.
    \label{fig:170814skymapsNoiseSub}}
\end{figure}
The impact of the subtraction of spectral lines and broadband noise is quite significant in the results of this study.  Fig.~\ref{fig:170814skymapsNoiseSub} shows the impact of the noise subtraction on the skymap computed for GW170814.  A systematic shift in position is clearly visible, and the the 1-$\sigma$ confidence region is reduced in size from 462 square degrees to 310 square degrees.  The strain data used for this comparison was compensated for all known time dependence.  A comparison of the same two data sets is also shown in Table~\ref{tab:170814PE}.  The rows labeled ``All" and ``All\_noclean" are the cleaned and uncleaned data, respectively.  The results show an 11\% increase in matched filter SNR due to the noise subtraction.  Additionally, the 1-$\sigma$ uncertainties of all parameters in the data set are significantly reduced.  Most surprising is the systematic change in luminosity distance $d_{\rm L}$ due to noise subtraction.  The majority of this impact comes from the removal of broadband noise caused by laser beam jitter in the H1 detector.  A similar comparison is shown for GW170817 in Table~\ref{tab:170817PE}, where the SNR increases by 6\% due to noise subtraction.




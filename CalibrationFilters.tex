\documentclass[VietsDissertation.tex]{subfiles}

\section{Principles of making FIR filters} \label{sec:filterBasics}

In order to understand the design of the digital FIR filters produced and used to calibrate Advanced LIGO data and the recent improvements that have been made to the design methods, a basic working knowledge of the principles of FIR filters is necessary.  A digital FIR filter is an array of numbers, called filter coefficients, that is applied to inputs using convolution, according to Eq.~\ref{eq:convolution}

\subsection{The ideal low- and high-pass filters} \label{ssec:lowHighPassFilter}

We begin by considering the low-pass filter, a filter designed to remove the content of a signal below a chosen cutoff frequency while leaving the higher-frequency content unchanged.  We define the ideal low-pass filter as the solution for ${\mathcal F}_{\rm lp}(f_{\rm cut}; \tau)$ in the equation
\begin{equation}
    \int_{-\infty}^{\infty} {\mathcal F}_{\rm lp}(f_{\rm cut}; \tau) \cos\left[2 \pi f (t - \tau) - \phi \right] d\tau = 
    \begin{cases}
        \cos(2 \pi f t - \phi),             & f < f_{\rm cut} \\
	\frac{1}{2} \cos(2 \pi f t - \phi), & f = f_{\rm cut} \\
        0,                                  & f > f_{\rm cut}
    \end{cases}
\end{equation}
The solution, called a sinc function, is
\begin{equation}
    {\mathcal F}_{\rm lp}(f_{\rm cut}; \tau) = f_{\rm cut} {\rm sinc}(f_{\rm cut} \tau)
\end{equation}
where
\begin{equation}
    {\rm sinc}(x) =
    \begin{cases}
        \frac{\sin(\pi x)}{\pi x}, & x \neq 0 \\
        1,                         & x = 0
    \end{cases}
\end{equation}
The filter ${\mathcal F}_{\rm lp}(f_{\rm cut}; \tau)$ is a perfect low-pass filter of infinite length with cutoff frequency $f_{\rm cut}$, defined for continuous input data.  In order to use this solution as a digital FIR filter, the length must be made finite using a point-by-point multiplication with some chosen window function.  Then, a new normalization must be applied to the filter by dividing the filter coefficients by the digital sample rate of the input data.

A low-pass filter can be used to produce a high-pass filter whose quality depends on the quality of the original low-pass filter, using a technique called spectral inversion.  To see this, imagine that we wish to simulate the effect of a high-pass filter by applying a low-ass filter to a data set, and then subtracting the low-pass filtered data from the original data set.  This would remove only low frequencies, leaving frequencies above the cutoff frequency of the low-pass filter unperturbed.  The same can by accomplished by negating each coefficient of a low-pass filter and adding one to the ``central" filter coefficient (i.e., the coefficient which multiplies input samples that have timestamps equal to the current output sample).

Construction of band-pass or band-stop filters can also be accomplished through convolution of any desired combination of low-pass and high-pass filters.  The length of the resulting filter in taps is the sum of the lengths of the original filters minus the number of convolutions.

\subsection{Applying time-domain window functions to filters} \label{ssec:windows}
A pivotal choice that must often be made in the construction of a digital FIR filter is how to window the filter in the time domain.  It is tempting to assume that a window function should alter the shape of the FIR filter as little as possible while avoiding edge effects.  However, choices made under this assumption often lead to spectral leakage and an ineffective attenuation beyond cutoff frequencies.  Since we wish to control the frequency response of the filter, it is useful to consider the effect of window functions in the frequency domain.  Applying a window to a filter in the time domain entails a point-by-point multiplication, which is a convolution when represented in the frequency domain.  The goal then is to determine what type of function should be convolved with the filter in the frequency domain.  Before the application of the window, the filter can be thought of as infinite in length and ideal in its frequency response.  Ideally, we would like to choose a window with a Fourier transform that looks like a Dirac delta function.  However, windows are finite in length and so cannot exactly replicate a Dirac delta function.  Appropriate choices must therefore be made based on the input data and the priorities for the frequency response of the filter being designed.

\subsection{Resampling of time-series data} \label{ssec:resampler}
As shown in Figs.~\ref{fig:GDSpipeline},~\ref{fig:DCSpipeline}, and~\ref{fig:cleaningPipeline}, time series data is frequently resampled in the \texttt{gstlal} calibration pipeline.  This is a necessity for the actuation path, since the FIR filters applied there are longer and need more agressive high-pass filtering, and filtering at the full $h(t)$ sample rate of 16384 Hz would lead to unmanageable computational cost.  Resampling is also important when demodulating the error signal and the injection channels needed to compute the TDCFs.  In order to accomplish these tasks with manageable computational cost, acceptable calibration accuracy, and minimal noise in the TDCFs, a very efficient resampling algorithm with effective anti-aliasing and anti-imaging is crucial.

The resampling algorithm in the \texttt{gstlal} calibration pipeline has a quality setting that can be adjusted based on the unique needs of each process.  For the simplest cases, data is downsampled by choosing every $n^{\rm th}$ sample or upsampled by copying each sample $n$ times, where $n$ is the ratio of sample rates.  For processes that need slightly higher quality, data can be downsampled by averaging $n$ samples or upsampled using linear extrapolation or a cubic spline.  For processes that demand high quality, a windowed sinc table is used to filter inputs.  When downsampling, the sinc table acts as an anti-aliasing filter that needs to be applied only once for every $n$ inputs, and is shifted by $n$ inputs after the production of each consecutive output sample.  When upsampling, the same sinc table acts as an anti-imaging filter sampled at the output sample rate.  When applied to inputs to produce an output sample, only every $n^{\rm th}$ tap is used on an input sample.  To compute the next sample, the sinc table is shifted relative the the inputs by one tap.  This combination of anti-aliasing and anti-imaging with the resampling process is essential to achieve the desired quality of calibrated data with manageable computational cost.

The anti-aliasing filter used in the downsampling to 16 Hz done during demodulation of the calibration lines is $\sim$12 s in length.  Prior to the development of the current resampling algorithm, this contributed 6 s of calibration latency.  Additionally, the TDCFs were upsampled to 16384 Hz using an anti-imaging filter that was also $\sim$12 s in length.  For this reason, the intrinsic latency of the \texttt{gstlal} calibration pipeline was $\sim$12 s during the early portion of O2.  Since temporal variations in the TDCFs are slow compared to this timescale, this latency was removed by the addition of an option in the resampling algorithm that allows the timestamps of output samples to be shifted so as to achieve a zero-latency filter.  This shift in timestamps is small compared to the shift in timestamps associated with the causal 20-second low-pass demodulation filter, the 128-second running median, and the 10-second running mean applied to the TDCFs.  With this latency removed, the highest latency process in the \texttt{gstlal} calibration pipeline is the application of the actuation filter.  The resulting improvement in latency is shown in Fig.~\ref{fig:latency}.

\section{Designing FIR filters to model the inverse sensing and actuation functions} \label{sec:calibrationFilters}
The calibration models for $A$ and $C^{-1}$ that the FIR filters are designed to implement are constructed in the frequency domain as described in Sections~\ref{sec:C} and~\ref{sec:A}.  The steps currently used to produce FIR filters based on the models of $A$ and $C^{-1}$ are similar to those described in \cite{hoft}, but are detailed here due to recent minor modifactions and for completeness:
\begin{enumerate}
\item {\it High-pass filter}.  Seismic noise in the raw data channels is too high at low frequencies to measure any GW signals, and the digital system has finite dynamic range.  Low frequencies are therefore rolled off by multiplying the frequency components from 4.5 Hz to 9 Hz by half of a Hann window raised to the fourth power.
\item {\it Low-pass filter in the sensing path}.  Since the inverse sensing function tends toward infinity at high frequencies, a low-pass filter is applied to the inverse sensing function to roll off high frequencies ($f \gtrapprox 6$ kHz) smoothly, in addition to the the anti-aliasing in the front end.  This is done by multiplying the high frequency components by half of a Hann window.
\item {\it Artificial delay}.  An artificial delay is added to the FIR filter that is equal to half of the length of the filter.  This delay is undone within the \texttt{gstlal} calibration pipeline by advancing the filter output by the same number of samples.  The reason for the delay is to center the FIR filter in time, avoiding edge effects while filtering and making the filter non-causal, with output depending on both past and future inputs.  The non-causal nature of the filters is necessary for physical reasons as well.  The response of the detector to GWs is causal since the detector is a physical system.  Therefore, when this response is inverted in the calibration to compute the GW signal from data in the error signal, the resulting filter is necessarily non-causal.
\item {\it Inverse Fourier transform}.  The inverse Fourier transform is computed to obtain the time-domain FIR filter.
\item {\it Time-domain window function}.  A window function is applied to the resulting time-domain FIR filter to ensure that the model and high-pass filter are well-represented by the filter.  Small systematic errors can be induced by the application of the window.  If necessary, these can be corrected by measuring the deviation of the filter's response from the frequency-domain model and compensating for this in the frequency-domain model before the above steps.
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/window_fft_plot.pdf}
    \caption[Comparison of window functions used in calibration filters]{
    A comparison of a 3.5-s Tukey window with $\alpha$ = 0.9 to a 3.5-s Kaiser window with $\beta$ = 28.  The plot on the left shows the windows in the time domain, while the center plot shows the Fourier transform.  The plot on the right is a zoomed-in view, showing the difference in the Fourier transform.  The much steeper drop in the Kaiser window from the main lobe leads to a dramatic improvement in the high-pass filtering in the calibration filters.
    \label{fig:windows}}
\end{figure}
During O3, this is being done in the actuation filters in the high-latency calibration to correct $\sim$1\% systematic errors below $\sim$20 Hz, which are likely caused by the steep increase in the magnitude of the actuation function at low frequencies and the width of the main lobe in the Fourier transform of the Kaiser window that is used (see Fig.~\ref{fig:windows}).
\end{enumerate}

\subsection{Attenuating low-frequency noise with minimal latency} \label{ssec:filterLatency}
A frequent request from downstream data analysts during O2 was to improve the quality of the high-pass filtering done in the \texttt{gstlal} calibration pipeline.  Increasing the length of the FIR filters is one way to improve the attenuation of low frequency noise, but it also adds unwanted latency to the low-latency $h(t)$ data and increases the computational cost of running the pipeline.  Moreover, a high-priority goal of O3 calibration is to reduce the calibration latency, which originates primarily from length of the actuation filters.  It is a challenging task to improve the high-pass filtering without increasing latency, or to reduce the latency without degrading the quality of the high-pass filtering.  One recent improvement in both high-pass filtering and calibration latency was accomplished with a more careful choice of window function to apply to the time-domain FIR filters.
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/L1GDS_latency_1235806316-28395.pdf}
    \caption[Time series of calibration latencies]{
    Time series of calibration latencies at L1, showing two significant improvements made since the beginning of O2.  Each point was computed by measuring the difference in the times when output data files exit the \texttt{gstlal} calibration pipeline and when input data files with the same timestamp enter the pipeline.  The time shown inclueds times when the detector is in low-noise state (green) and times when it is not (red), as indicated by the bar at the bottom.  The median latencies $\mu_{1/2}$ and standard deviations $\sigma$ are shown for each data set.
    \label{fig:latency}}
\end{figure}
During O2, a Tukey window was used with $\alpha = 0.9$ for the last step in the procedure above for making filters.  For a window with $N$ samples, this is defined by
\begin{equation}
    W_{\rm Tukey}(n) =
    \begin{cases}
        \sin^2\left(\frac{\pi n}{\alpha(N - 1)}\right),                       & 0 \leq n < \alpha \frac{N - 1}{2} \\
        1,                                                                                    & \alpha \frac{N - 1}{2} \leq n \leq (2 - \alpha) \frac{N - 1}{2} \\
        \cos^2\left(\frac{\pi n}{\alpha (N - 1)} - \frac{\pi}{\alpha} + \frac{\pi}{2}\right), & (2 - \alpha) \frac{N - 1}{2} < n \leq N - 1
    \end{cases}
\end{equation}
This window has recently been replaced with a Kaiser window, which approximates a window function that maximizes the energy concentration in the main lobe of the Fourier transform.
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/Cinv_HPfilt.pdf}
    \caption[Additional high-pass filter in the inverse sensing path]{
    A block diagram showing a method by which to high-pass filter the inverse sensing path below the full $h(t)$ sample rate without losing any high-frequency information.
    \label{fig:resHighpassTrick}}
\end{figure}
The Kaiser window is defined by
\begin{equation}
    W_{\rm Kaiser}(n) = \frac{I_0\left(\beta \sqrt{1 - \left(\frac{2n}{N - 1} - 1\right)^2}\right)}{I_0(\beta)} \, ,
\end{equation}
where $I_0$ is the zeroth order modified Bessel function of the first kind.  Use of the Kaiser window has dramatically improved the high-pass filtering due to the fact that the magnitude of the Fourier transform quickly drops many orders of magnitude as one moves away from the main lobe.  The roll-off of the digital calibration filters below 9 Hz is therefore much steeper using a Kaiser window.
The actution filter has been reduced in length from 6 s to 3.5 s, using $\beta$ = 28.  The 1-second inverse sensing filter is also windowed with a Kaiser window with $\beta$ = 10.  Time- and frequency-domain comparisons of the Tukey window to the Kaiser window are shown in Fig.~\ref{fig:windows}.  The O2-style filters and the new O3 filters were used to calibrate 8 hours of L1 data prior to O3.  The resulting improvement in intrinsic calibration pipeline latency is shown in Fig.~\ref{fig:latency}.  This plot was generated using the L1 computing cluster rather than the DMT.  The improvement in calibration latency due the the shorter actuation filters is slightly less significant than expected (latency is expected to be less than 3 s), possibly due in part to the fact that numerous other jobs were running at the time of this test.  The dramatic increase in latency just before 4 hours is due to raw data drop-outs described in~\ref{ssec:lalInsertGap}.  This is unrelated to the latency caused by the lengths of the filters, but it needs to be addressed.  Another plot of calibration latency for H1 data using similar configurations can be seen in Fig.~\ref{fig:noiseSubLatency}.

Although the low-frequency noise in the actuation path is louder than that in the sensing path, attenuating this noise in the sensing path has the additional challenge that the data is filtered at the full $h(t)$ sample rate of 16384 Hz.  This is necessary because the error signal $d_{\rm err}$ contains significant signal content up to the Nyquist frequency.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/L1_1235815424_1235819520_spectrum_comparison.pdf}
    \caption[Amplitude spectral densities showing improvements in high-pass filtering]{
    A comparison of 3 amplitude spectral densities, showing improvements in the high-pass filters used in the \texttt{gstlal} calibration pipeline.  The improvements are due to the use of a Kaiser window instead of a Tukey window in the filters, as well as the use of an additional high-pass filter in the sensing path.
    \label{fig:highpassASDs}}
\end{figure}
The challenge of attenuating this noise therefore arises not due to the need to reduce latency, but due to the computational cost of filtering a 16384-Hz time series with a long FIR filter.
At times, low-frequency noise in the sensing path dominates the final signal content in $h(t)$ below 10 Hz due to the fact that the actuation filter is 3.5 s in length and the inverse sensing filter is only 1.0 s in length.  To attenuate this noise without dramatically increasing computational cost, a method is used to apply a separate high-pass filter to the inverse sensing path at a lower sample rate while still preserving the content of the signal up to the Nyquist rate.  The method, depicted in Fig.~\ref{fig:resHighpassTrick}, is to tee the sensing path, downsample one copy to apply a low-pass filter, upsample the low-passed data, and subtract it from the original data before applying the inverse sensing filter.  The additional high-pass filter currently has a length of 2.5 s so that the total filtering latency of the sensing path is equal to that of the actuation path, and it is windowed using a Kaiser window with $\beta$ = 20.
The improvements in low-frequency noise attenuation due to the use of a 3.5-second Kaiser window instead of a 6-second Tukey window in the actuation filter, as well as the inclusion of the additional high-pass filter in the sensing path, are shown in Fig.~\ref{fig:highpassASDs}.  Amplitude spectral densities computed from 4096 s of low-noise L1 data taken prior to O3 are plotted, showing an improvement in the stop band of about 6 orders of magnitude over the O2-style filters.

\subsection{Accuracy of the filters' representation of the response function} \label{ssec:filterResponse}
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/L1DCS_1235491416_npz_response_filters_transfer_function_1235815169-4584_ratio.pdf}
    \caption[Response function of the calibration filters]{
    A comparison of the response of the calibration filters to the frequency-domain modeled response function $\tilde{R}^{\rm model}(f)$.  The transfer function $\widetilde{\Delta L}_{\rm free}(f) / \tilde{d}_{\rm err}(f)$ was taken using $\sim$4500 s of L1 data on March 5, 2019, shortly prior to O3.  The front-end calibration filters at this time show more systematic error than is typical.  The low-latency \texttt{gstlal} calibration data product (GDS) and the high-latency \texttt{gstlal} calibration data product (DCS) are shown as well, each showing the expected improvements.  The noise in the plots on the right from 15 - 20 Hz is most likely due to the 4 calibration lines in that band.
    \label{fig:filtersResponse}}
\end{figure}
A useful test to evaluate the accuracy of the filters' application of the time-independent response function $\tilde{R}^{\rm model}(f)$ is done by empirically evaulating the transfer function between the error signal $d_{\rm err}(t)$ and the time series $\Delta L_{\rm free}(t)$ reconstructed by the calibration pipelines, and comparing this to the frequency-domain model $\tilde{R}^{\rm model}(f)$.  Note that such tests of the accuracy of the filters necessarily do not compensate for any time dependence.  Fig.~\ref{fig:filtersResponse} shows such a comparison, using three different calibration products: the front-end calibration data product produced using IIR filters, the low-latency strain data product produced by the combined action of the front end (IIR filters) and the \texttt{gstlal} calibration pipeline (FIR filters), and the high-latency strain data product produced entirely using FIR filters in the \texttt{gstlal} calibration pipeline.  The data was taken at L1 from March 5, 2019, prior to O3.  Note the $\sim$1\% discrepancy below 1 kHz in the low-latency \texttt{gstlal} calibration.  This is due to high-frequency poles applied in the actuation in the front-end calibration pipeline but not corrected by the FIR filters in the \texttt{gstlal} calibration pipeline.  This discrepancy is not present in high-latency calibrated data.  The low-latency $h(t)$ data at H1 also does not show this discrepancy because these poles are not applied in the front-end calibration pipeline at H1.  It is a priority to begin correcting this systematic error in the \texttt{gstlal} calibration pipeline during O3.



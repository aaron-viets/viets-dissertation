\documentclass[VietsDissertation.tex]{subfiles}

The \texttt{gstlal} calibration pipeline forms the second stage of the low-latency calibration procedure, correcting known systematic errors remaining in the front-end calibration.  It is also used in high latency for the entire calibration procedure.  Unlike the front-end calibration pipeline, the \texttt{gstlal} calibration pipeline is run on separate machines outside of the front end, and it uses FIR filters instead of IIR filters to apply the calibration model to the input data.  The primary advantage of using FIR filters is that an FIR filter can be designed to implement virtually any frequency-domain model.  Additionally, FIR filters make it possible to fulfill the requirement that all output must be independent of the start time of the pipeline.  The output of an FIR filter depends on a fixed finite number of input samples, while the impact of an input sample to an IIR filter typically decays over time, but does not disappear completely.

\section{Use of \texttt{GStreamer} and \texttt{gstlal} in calibration} \label{sec:gstreamer}
Advanced LIGO strain data is processed and stored as double-precision floating-point numbers sampled at 16384 Hz and contains potentially useful information from 10 Hz to 8192 Hz.  This range of frequency overlaps significantly with the human hearing range of 20 Hz to 20 kHz.  Because of this similarity, it is possible to take advantage of software that is developed for the purpose of processing audio signals.  The \texttt{gstlal} calibration pipeline uses the open-source audio and video streaming software package \texttt{GStreamer} \cite{gstreamer}, provided under the \texttt{GNU} license \cite{gnu}.  \texttt{GStreamer} is a pipeline-based multimedia framework written in C, and uses a type system based on \texttt{GObject} \cite{gobject} and \texttt{GLib} \cite{glib}.  \texttt{GStreamer} provides bindings for Python, allowing pipelines such as the \texttt{gstlal} calibration pipeline to be constructed in Python.  The \texttt{GStreamer} package used for calibration is called \texttt{gstlal-calibration} \cite{gstlalcalibration}, and is a part of the \texttt{GstLAL} project \cite{gstlal}, which wraps LIGO Algorithm Library (\texttt{LAL}) software \cite{LAL} with \texttt{GStreamer}.  The obvious advantages of using \texttt{GStreamer} are the many built-in features for low-latency streaming of audio-like data.  A disadvantage is that, since \texttt{GStreamer} is not primarily designed to process LIGO data, some software package updates cause problems in the \texttt{gstlal} calibration pipeline that can require urgent bug fixes.  The development of many customized \texttt{GStreamer} elements written for the purpose of calibration has improved the \texttt{gstlal} calibration pipeline's immunity to such problems.

\section{Operation procedures in the \texttt{gstlal} calibration pipeline} \label{sec:gstlalCalibrationProcedure}
Although significant differences between low-latency and high-latency calibration procedures are noted in Sections~\ref{ssec:lowLatencyGstlalCalibration} and~\ref{ssec:highLatencyGstlalCalibration}, similarities in the procedures allow for a general description, given below.

\subsection{Filling in missing raw data} \label{ssec:lalInsertGap}
For various reasons, and especially in low latency, raw data is occasionally missing as input to the \texttt{gstlal} calibration pipeline.  The frequency of raw data dropouts and the amount of missing data varies greatly, but most of the time these dropouts occur at a frequency of a few times per week and last only a few seconds.  On rare occasions however, hours of raw data can be dropped.  These are generally much less common when the detector is in a nominal low-noise configuration, but occasionally occur then as well.  Since downstream data analyses rely on a continuous $h(t)$ data stream, it is essential that the \texttt{gstlal} calibration pipeline fills in this missing data.  In most channels, these are filled in with zeros.  The only exceptions are the channels that report uncertainty in the calibration lines described in Section~\ref{ssec:kappaSmoothing}, which are filled in with ones.\footnote{These channels are used to determine when to gate the calculation of the TDCFs, and a value of zero during a data dropout would falsly indicate that the calculation of the TDCFs is reliable.}  The impact of these dropouts on calibration latency can be seen in Fig.~\ref{fig:latency}, where a $\sim$20-minute stretch of low-noise data is impacted by frequent short dropouts.  It is a priority to address this latency during O3, especially since, after a long stretch of missing raw data, it can take the \texttt{gstlal} calibration pipeline a considerable time to ``catch up" to normal latency levels as it furiously calibrates worthless fake data.  One possible method to prevent these occasional large increases in latency would be to track the time since raw data has arrived in the \texttt{gstlal} calibration pipeline.  Anytime the pipeline has waited longer than some chosen threshold, it could begin filling in the missing data.  This would prevent the pipeline from falling behind, providing a more consistent $h(t)$ time series for downstream analysis and saving the time currently needed to catch up after a long data dropout.

\subsection{Computing the time-dependent correction factors} \label{ssec:gstlalTDCFs}
Despite the fact that the front-end calibration pipeline computes the TDCFs, the \texttt{gstlal} calibration pipeline computes and utilizes the TDCFs independently.  This is due in part to the fact that the noise attenuation process described in Section~\ref{ssec:kappaSmoothing} has not yet been fully developed and tested in the front-end calibration pipeline, and to the fact that the \texttt{gstlal} calibration pipeline also needs to compute the TDCFs in high latency.  It additionally allows results to be compared between the front end and the \texttt{gstlal} calibration pipeline.  The calculation of the TDCFs in the \texttt{gstlal} calibration pipeline begins with measurement of the calibration lines, accomplished using demodulation as described in Section~\ref{ssec:demodulation}.  The data is downsampled to 16 Hz in this process before the TDCFs are computed, preventing unmanageable computational cost.  The subsequent calculation of the TDCFs is described in detail in Section~\ref{sec:computingTDCFs}.

\subsection{Applying FIR filters to compute $h(t)$} \label{ssec:gstlalHoft}
The \texttt{gstlal} calibration pipeline applies time-domain FIR filters modeling the actuation function $\tilde{A}$ at each stage, described by Eq.~\ref{eq:A}, and the inverse sensing function $\tilde{C}^{-1}$, described by Eq.~\ref{eq:C}, to the actuation and inverse sensing paths separately, before adding the components to produce $\Delta L_{\rm free}(t)$.  During O2, the scalar correction factors $\kappa_{\rm T}$ and $\kappa_{\rm C}$, as well as the factor $\kappa_{\rm PU}$ (estimate of the combined impact of $\kappa_{\rm P}$ and $\kappa_{\rm U}$) were applied as multiplicative factors to the components of $\Delta L_{\rm free}$ after the static model filters were applied:
\begin{align}
    h(t) = \frac{1}{L} \Biggl(\frac{1}{\kappa_{\rm C}(t)} C^{-1, {\rm model}} \ast d_{\rm err}(t) &+ \kappa_{\rm T}(t) A^{\rm model}_{\rm T} \ast d_{\rm ctrl}(t) \Biggr. \\
         \Biggl. &+ \kappa_{\rm PU}(t) A^{\rm model}_{\rm PU} \ast d_{\rm ctrl}(t) \Biggr) \, . \nonumber
\end{align}
During O3, it will be possible to use adaptive filtering techniques to apply time-dependent filters accounting for all parameterized time dependence in the \texttt{gstlal} calibration pipeline, as described in detail in Section~\ref{ssec:adaptiveFiltering}, the result being
\begin{equation}
    h(t) = \frac{1}{L} \left(C^{-1}(t) \ast d_{\rm err}(t) + A_{\rm T}(t) \ast d_{\rm ctrl}(t) + A_{\rm P}(t) \ast d_{\rm ctrl}(t) + A_{\rm U}(t) \ast d_{\rm ctrl}(t)\right) \, .
\end{equation}

\subsection{The calibration state vector} \label{ssec:calibStateVector}
The \texttt{gstlal} calibration pipeline computes a bitwise state vector using 32-bit unsigned integers sampled at 16 Hz.  The calibration state vector contains information about the integrity of the $h(t)$ data product at each moment of time, as well as additional information about the state of the calibration.  Definitions of each bit in the O3 calibration state vector are shown in Table~\ref{tab:calibStateVector}.
\begin{table*}[!t]
\caption[Calibration state vector bits]{\label{tab:calibStateVector} A summary of the meaning of each bit in the calibration state vector during O3.}
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular} { || c | c | c ||}
\hline
\textbf{bit} & \textbf{Short descriptor} & \textbf{Long descriptor} \\
\hline
\hline
0 & HOFT\_OK & $h(t)$ was successfully computed \\
1 & OBS\_INTENT & interferometer is in ``observation intent" mode \\
2 & OBS\_READY & interferometer is in ``observation ready" mode \\
3 & FILTERS\_OK & calibration filters settled in \\
4 & NO\_GAP & Input data is present and no underflows or overflows \\
5 & NO\_STOCH\_HW\_INJ & No stochastic hardware injections present \\
6 & NO\_CBC\_HW\_INJ & No compact binary coalescence hardware injections present \\
7 & NO\_BURST\_HW\_INJ & No burst hardware injections present \\
8 & NO\_DETCHAR\_HW\_INJ & No detector characterization hardware injections present \\
9 & KAPPA\_T\_SMOOTH\_OK & $\kappa_{\rm T}$ output is in expected range \\
10 & KAPPA\_P\_SMOOTH\_OK & $\kappa_{\rm P}$ output is in expected range \\
11 & KAPPA\_U\_SMOOTH\_OK & $\kappa_{\rm U}$ output is in expected range \\
12 & KAPPA\_C\_SMOOTH\_OK & $\kappa_{\rm C}$ output is in expected range \\
13 & F\_CC\_SMOOTH\_OK & $f_{\rm cc}$ output is in expected range \\
14 & F\_S\_SMOOTH\_OK & $f_{\rm s}$ output is in expected range \\
15 & Q\_INV\_SMOOTH\_OK & $Q^{-1}$ of SRC output is in expected range \\
16 & SUS\_LINE3\_COH\_OK & Coherence of test actuator line is acceptable \\
17 & SUS\_LINE2\_COH\_OK & Coherence of penultimate actuator line is acceptable \\
18 & SUS\_LINE1\_COH\_OK & Coherence of upper intermediate actuator line is acceptable \\
19 & PCALY\_LINE1\_COH\_OK & Coherence for first Pcal line is acceptable \\
20 & PCALY\_LINE2\_COH\_OK & Coherence for second Pcal line is acceptable \\
21 & PCALY\_LINE4\_COH\_OK & Coherence for lowest Pcal line is acceptable \\
22 & D\_EPICS\_MATCH & TDCF reference factors for digital filter $D$ agree \\
23 & A\_EPICS\_MATCH & TDCF reference factors for actuation function $A$ agree \\
24 & C\_EPICS\_MATCH & TDCF reference factors for sensing function $C$ agree \\
25 & MISC\_EPICS\_MATCH & Miscellaneous TDCF reference factors match \\
26 & LINE\_SUBTRACTION\_OK & Subtraction of calibration lines is working \\
27 & NOISE\_SUBTRACTION\_OK & Subtraction of broadband noise is working \\
28 & NOISE\_SUBTRACTION\_GATE & Transfer function calculation is not being gated \\
\hline
\end{tabular}}
\end{table*}
Bit 0 indicates that the $h(t)$ data currently produced is accurate and suitable for analysis.  It is computed as the logical AND of bits 2, 3, and 4, plus any of bits 9 through 15 corresponding to a time-dependent correction that is being compensated for in $h(t)$.  Bits 1 and 2 are read in from the Guardian, a front-end platform consisting of distributed, independent, state machine automation nodes organized hierarchically for full detector control \cite{guardian}.  Bit 1 tells whether the operator in the control room has determined that there are no ongoing commissioning activities and the data being produced is science-quality.  Bit 2 indicates that the detector is in a nominal low-noise configuration.  Bit 3 indicates that the calibration filters have settled, that is, all of the input data filtered to produce the current output samples is low-noise data that does not include any data dropouts.  Bit 4 indicates that there are no raw data dropouts or arithmetic underflow or overflow inputs to the pipeline.  Bits 5 through 8 indicate that there are no hardware injections present in the current $h(t)$ data.  Bits 9 through 15 indicate that the computed TDCFs are within an expected range.  Bits 16 through 21 indicate that the coherence of the calibration lines between the injection channels and the error signal are acceptable based on a predetermined threshold, as described in Section~\ref{ssec:kappaSmoothing}.  Bits 22 through 25 indicate that the two sets of reference model values used to compute the TDCFs agree, one in the front end and the other in the filters file read in by the \texttt{gstlal} calibration pipeline.  The front-end reference model values are used in low latency, while the values stored in the filters file are used in high latency.  Bits 26 and 27 indicate that subtraction of spectral lines and noise is successfully reducing the RMS of the strain signal in the frequency bands where the impact is expected to be significant.  Bit 28 indicates that the calculation of transfer functions used for broadband noise subtraction is running, and not being halted due to increased levels of detector noise.  The spectral line and noise subtraction is described in detail in Chapter~\ref{ch:NoiseSubtraction}.

\section{The low-latency \texttt{gstlal} calibration pipeline} \label{ssec:lowLatencyGstlalCalibration}
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/GDS_pipeline_diagram.pdf}
    \caption[Block diagram of the low-latency \texttt{gstlal} calibration pipeline]{
    A simplified block diagram showing the workflow of the low-latency \texttt{gstlal} calibration pipeline.  It receives as inputs the calibrated components of $\Delta L_{\rm free}$ computed by the front-end calibration pipeline and applies the FIR filters $A_{\rm corr}$ and $C^{-1}_{\rm corr}$ to correct known systematic errors.  Solid lines represent the continuous flow of digital time-series data, while the dashed lines represent the periodic passing of new FIR filters from elements that compute them to elements that use them to filter input data.  Details of the calculation of the TDCFs and the calibration state vector are not shown.  Fig.~\ref{fig:cleaningPipeline} shows the broadband noise subtraction in the \texttt{gstlal} calibration pipeline.
    \label{fig:GDSpipeline}}
\end{figure}
The low-latency \texttt{gstlal} calibration pipeline, also called the GDS (global diagnostic system) calibration pipeline, is run on a machine at the detector sites called a data monitoring tool (DMT).  The DMTs receive raw data in very low latency from a frame broadcaster.  The raw data is written to a shared memory partition where it is read in by the \texttt{gstlal} calibration pipeline.  Calibrated GW frames are then written to another shared memory partition to be sent to downstream processes and distributed.

The primary purpose of the low-latency \texttt{gstlal} calibration pipeline is to correct systematic errors remaining after the IIR filtering done in the front end.  This is especially impactful at high frequencies, due to the presence of poles above the Nyquist rate that are difficult to model using IIR filters, and the current inability to apply a phase advance in the real-time code of the front-end computing system.  Additionally, the \texttt{gstlal} calibration pipeline computes and compensates for the TDCFs, improving calibration accuracy by up to 20\% in the detection band.  The front-end calibration pipeline is also capable of tracking and compensating for the TDCFs, but the \texttt{gstlal} calibration pipeline does not currently read in the TDCF-compensated channels from the front end, mainly due to the fact that more development and testing is needed in the attenuation of noise in the front-end TDCFs.  Fig.~\ref{fig:GDSpipeline} is a block diagram showing the workflow of the low-latency \texttt{gstlal} calibration pipeline.

Due to the front-end calibration pipeline's easy access to detector state information and the ability to make changes to calibration parameters quickly, it is a long-term goal to move more of the low-latency calibration procedure into the front end.  However, as of the beginning of O3, there is still much to be done in the \texttt{gstlal} calibration pipeline in low latency.

\section{The high-latency \texttt{gstlal} calibration pipeline} \label{ssec:highLatencyGstlalCalibration}
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/DCS_pipeline_diagram.pdf}
    \caption[Block diagram of the high-latency \texttt{gstlal} calibration pipeline]{
    A simplified block diagram showing the workflow of the high-latency \texttt{gstlal} calibration pipeline.  It applies FIR filters to the error signal $d_{\rm err}$ and control signal $d_{\rm ctrl}$ to produce calibrated strain data.  Solid lines represent the continuous flow of digital time-series data, while the dashed lines represent the periodic passing of new FIR filters from elements that compute them to elements that use them to filter input data.  Details of the calculation of the TDCFs and the calibration state vector are not shown.  Fig.~\ref{fig:cleaningPipeline} shows the broadband noise subtraction in the \texttt{gstlal} calibration pipeline.
    \label{fig:DCSpipeline}}
\end{figure}
The high-latency calibration pipeline, also called the DCS (data and computing systems) calibration pipeline, is run using many 4096-second jobs in parallel on the LIGO Data Grid computing cluster at the California Institute of Technology \cite{ldg}.  The raw data is read from raw GW frame files, each containing 64 s of raw data, and calibrated data is then written to another set of GW frame files, each with 4096 s of calibrated data.  In high latency, the entire calibration procedure is done in the \texttt{gstlal} calibration pipeline, using as inputs the error and control signals, $d_{\rm err}$ and $d_{\rm ctrl}$.  The full time-dependent inverse sensing and actuation functions are applied as FIR filters to $d_{\rm err}$ and $d_{\rm ctrl}$ to produce the calibrated strain product.  Fig.~\ref{fig:DCSpipeline} is a block diagram showing the workflow of the high-latency \texttt{gstlal} calibration pipeline.


